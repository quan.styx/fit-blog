import Blog from 'pages/blog'
import Landing from 'pages/landing'
import { useEffect } from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { useAppDispatch } from './store'
import { loginWithGoogleFirebase } from './pages/authSlice'
function App() {
  const dispatch = useAppDispatch()
  const name = sessionStorage.getItem('userName')
  const email = sessionStorage.getItem('userName')
  const token = sessionStorage.getItem('userName')
  const userId = sessionStorage.getItem('userName')

  useEffect(() => {
    if (name && email && token && userId) {
      dispatch(loginWithGoogleFirebase({ userName: name, email: email, token: token, userId: userId }))
    }
  }, [])
  return (
    <div className='App'>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Landing />} />
          <Route index element={<Landing />} />
          <Route path='blog'>
            <Route index element={<div>Blog not found!!!</div>} />
            <Route path=':id' element={<Blog />} />
          </Route>
          <Route path='*' element={<div>Not found!!!</div>} />
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
