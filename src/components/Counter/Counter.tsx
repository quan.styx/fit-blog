import Count from 'components/Count/Count'
import { useRef } from 'react'
import { useInView } from 'react-intersection-observer'
export default function Counter() {
  const viewCounted = useRef(true)

  const { ref: myRef, inView: myElementIsVisible } = useInView({ triggerOnce: true })
  /* ===================================
    Counter on scroll section
    ====================================== */
  function reveal() {
    let reveals = document.querySelectorAll('.reveal')
    for (let i = 0; i < reveals.length; i++) {
      let windowHeight = window.innerHeight
      let elementTop = reveals[i].getBoundingClientRect().top
      let elementVisible = 150

      if (elementTop < windowHeight - elementVisible) {
        reveals[i].classList.add('active')
      } else {
        reveals[i].classList.remove('active')
      }
    }
  }
  window.addEventListener('scroll', reveal)

  return (
    <div ref={myRef} className='fitzaro_coundown p-10' id='countdown'>
      <div className='parallax-overlay' />
      <div className='container'>
        <div className='row'>
          <div className='col-12 col-sm-6 col-lg-3'>
            <div
              className='countdown_formula text-center'
              data-aos='flip-up'
              data-aos-easing='linear'
              data-aos-duration={1000}
            >
              <div className='counter_image'>
                <img src='assets/images/svg/headphone.svg' alt='Headphone' className='img-fluid' />
              </div>
              <h4 className='m-0'>
                {/* <CountUp scrollSpyOnce={true} className='coundown-number' data-number={75} end={75} redraw={false}>
                  {({ countUpRef, start }) => (
                    <VisibilitySensor onChange={start} delayedCall>
                      <span className='coundown-number' ref={countUpRef} />
                    </VisibilitySensor>
                  )}
                </CountUp> */}
                {myElementIsVisible && viewCounted.current && (
                  <Count className='coundown-number' value={0} end={75} active={true} />
                )}
              </h4>
              <div className='sub_info'>Expert Trainer</div>
            </div>
          </div>
          <div className='col-12 col-sm-6 col-lg-3'>
            <div
              className='countdown_formula text-center'
              data-aos='flip-up'
              data-aos-easing='linear'
              data-aos-duration={1000}
            >
              <div className='counter_image'>
                <img src='assets/images/svg/rating.svg' alt='Rating' className='img-fluid' />
              </div>
              <h4 className='m-0'>
                {/* <CountUp enableScrollSpy={true} className='coundown-number' data-number={120} end={120}></CountUp> */}
                {myElementIsVisible && viewCounted.current && (
                  <Count className='coundown-number' value={0} end={120} active={true} />
                )}
              </h4>
              <div className='sub_info'>Client Feedback</div>
            </div>
          </div>
          <div className='col-12 col-sm-6 col-lg-3'>
            <div
              className='countdown_formula text-center'
              data-aos='flip-up'
              data-aos-easing='linear'
              data-aos-duration={1000}
            >
              <div className='counter_image'>
                <img src='assets/images/svg/gym.svg' alt='Gym' className='img-fluid' />
              </div>
              <h4 className='m-0'>
                {/* <CountUp enableScrollSpy={true} className='coundown-number' data-number={100} end={100}></CountUp> */}
                {myElementIsVisible && viewCounted.current && (
                  <Count className='coundown-number' value={0} end={100} active={true} />
                )}
              </h4>
              <div className='sub_info'>Total Branches</div>
            </div>
          </div>
          <div className='col-12 col-sm-6 col-lg-3'>
            <div
              className='countdown_formula text-center'
              data-aos='flip-up'
              data-aos-easing='linear'
              data-aos-duration={1000}
            >
              <div className='counter_image'>
                <img src='assets/images/svg/winning.svg' alt='Winning' className='img-fluid' />
              </div>
              <h4 className='m-0'>
                {/* <CountUp enableScrollSpy={true} className='coundown-number' data-number={70} end={70}></CountUp> */}
                {myElementIsVisible && viewCounted.current && (
                  <Count className='coundown-number' value={0} end={70} active={true} />
                )}
              </h4>
              <div className='sub_info'>Award Winning</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
