import Tilt from 'react-parallax-tilt'
function Video() {
  return (
    <div className='fitzaro_service p-10' id='service'>
      <div className='background_shape animate-this'>
        <img src='assets/images/svg/service-shape.svg' alt='service-shape' />
      </div>
      <div className='wrap_detail'>
        <div className='container'>
          <div className='row'>
            <div className='col-12 col-lg-6 col-md-6'>
              <Tilt>
                <div className='service_img' data-aos='fade-right' data-aos-duration={1000}>
                  <div className='triangle-top' />
                  <div className='triangle-bottom' />
                  <img src='assets/images/service/service-img1.png' alt='service' className='img-fluid' />
                </div>
              </Tilt>
            </div>
            <div className='col-12 col-lg-6 col-md-6'>
              <div className='services-detail'>
                <span className='sub_heading'>Our Services</span>
                <h2 className='main_heading m-0'>Solutions for moving better and feeling a healthier</h2>
                <p className='m-0 p-8'>
                  Sed enim purus diam faucibus amet a dolor sed. Orci porttitor fames sed ullamcorper et nunc dui. Sit
                  eget morbi lorem non arcu faucibus cras. Ipsum sapien pretium sollicitudin et tortor, nunc tristique
                  sociis. At rhoncus mollis tristique integer ut at. Ac eleifend enim ipsum posuere lorem congue.
                  Euismod turpis viverra diam neque sit. Id sit id magna a odio tincidunt orci, donec ultricies.
                  <br />
                  <br />
                  Commodo ut eget interdum id in in non elementum feugiat. Massa felis nisl aliquam, vestibulum et,
                  sollicitudin. Amet est elit tincidunt dui dui non egestas lorem malesuada id ipsum.
                </p>
                <a className='button fitzaro_btn' href='#appointment'>
                  Book Appointment
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='services_tool'>
        <div className='container'>
          <div className='row ordered-1' data-aos='fade-down' data-aos-easing='linear' data-aos-duration={1000}>
            <div className='col-sm-6 col-lg-4 box-tool'>
              <a href='weight_lifting.html'>
                <div className='fitzaro_tools'>
                  <img src='assets/images/svg/weight.svg' alt='Weight Lifting' className='img-fluid' />
                  <h3 className='m-0'>Weight Lifting</h3>
                </div>
                <div className='order-number' />
                <img
                  src='assets/images/svg/weight-white.svg'
                  alt='Weight Lifting1'
                  className='img-fluid exercise-img'
                />
              </a>
            </div>
            <div className='col-sm-6 col-lg-4 box-tool'>
              <a href='aerobics.html'>
                <div className='fitzaro_tools'>
                  <img src='assets/images/svg/body.svg' alt='Aerobics' className='img-fluid' />
                  <h3 className='m-0'>Aerobics</h3>
                </div>
                <div className='order-number' />
                <img src='assets/images/svg/body-white.svg' alt='Aerobics1' className='img-fluid exercise-img' />
              </a>
            </div>
            <div className='col-sm-6 col-lg-4 box-tool'>
              <a href='cardio.html'>
                <div className='fitzaro_tools'>
                  <img src='assets/images/svg/crosfit.svg' alt='Cardio Tools' className='img-fluid' />
                  <h3 className='m-0'>Cardio</h3>
                </div>
                <div className='order-number' />
                <img src='assets/images/svg/crosfit-white.svg' alt='Cardio' className='exercise-img img-fluid' />
              </a>
            </div>
            <div className='col-sm-6 col-lg-4 box-tool'>
              <a href='boxing_punch.html'>
                <div className='fitzaro_tools'>
                  <img src='assets/images/svg/boxing-punch.svg' alt='Boxing Punch' className='img-fluid' />
                  <h3 className='m-0'>Boxing Punch</h3>
                </div>
                <div className='order-number' />
                <img
                  src='assets/images/svg/boxing-punch-white.svg'
                  alt='boxing-punch'
                  className='exercise-img img-fluid'
                />
              </a>
            </div>
            <div className='col-sm-6 col-lg-4 box-tool'>
              <a href='power_yoga.html'>
                <div className='fitzaro_tools'>
                  <img src='assets/images/svg/yoga.svg' alt='Power Yoga' className='img-fluid' />
                  <h3 className='m-0'>Power Yoga</h3>
                </div>
                <div className='order-number' />
                <img src='assets/images/svg/yoga-white.svg' alt='yoga' className='exercise-img img-fluid' />
              </a>
            </div>
            <div className='col-sm-6 col-lg-4 box-tool'>
              <a href='zumba_class.html'>
                <div className='fitzaro_tools'>
                  <img src='assets/images/svg/zumba.svg' alt='Zumba Class' className='img-fluid' />
                  <h3 className='m-0'>Zumba Class</h3>
                </div>
                <div className='order-number' />
                <img src='assets/images/svg/zumba-white.svg' alt='zumba' className='exercise-img img-fluid' />
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Video
