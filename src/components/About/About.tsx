import React from 'react'

function About() {
  return (
    <div className='about-section p-10' id='about'>
      <div className='fitzaro_health'>
        <div className='container'>
          <div className='row'>
            <div className='col-12 col-lg-6 col-md-6 about_img' data-aos='fade-right' data-aos-duration={1000}>
              <div className='about_text'>
                <img src='/assets/video/text-effect.gif' alt='text-effect' />
              </div>
              <img src='assets/images/about/about-img.png' alt='About_gym img' className='img-fluid abo-img' />
            </div>
            <div className='col-12 col-lg-6 col-md-6'>
              <div className='abo_content' data-aos='fade-left' data-aos-duration={1000}>
                <span className='sub_heading'>About Us</span>
                <h2 className='main_heading m-0'>Giving Good Health To Good People</h2>
                <p className='about_detail m-0'>
                  Pretium suscipit feugiat ultrices morbi sagittis turpis amet enim. Velit lorem id fringilla quisque
                  ullamcorper. Gravida nulla elementum quisque platea id aliquet. Et auctor pharetra a turpis tincidunt
                  sem. Enim amet, malesuada integer nec viverra.
                </p>
                <div className='box_detail'>
                  <div className='list_icon'>
                    <i className='ri-check-line' />
                    <p className='check_detail m-0'>Duis quis odio quis dui sagittis laoreet.</p>
                  </div>
                  <div className='list_icon'>
                    <i className='ri-check-line' />
                    <p className='check_detail m-0'>Suspendisse tempus felis a libero mollis ultrices.</p>
                  </div>
                  <div className='list_icon'>
                    <i className='ri-check-line' />
                    <p className='check_detail m-0'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  </div>
                  <div className='list_icon'>
                    <i className='ri-check-line' />
                    <p className='check_detail m-0'>Suspendisse tempus felis a libero mollis ultrices.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='row build_best' data-aos='zoom-in-up' data-aos-easing='linear' data-aos-duration={1000}>
            <div className='col-12 col-md-6 col-lg-6 p-30'>
              <div className='services_box'>
                <div className='service-content'>
                  <div className='service_wrappe'>
                    <div className='instructor'>
                      <div className='service_ic'>
                        <img src='/assets/images/svg/bounding.svg' alt='Bounding-box' />
                      </div>
                      <div className='service_title'>Dedicated Services</div>
                    </div>
                    <p className='blogs-block m-0 text-start'>
                      Amet lectus ut ullamcorper dui convallis dictumst orci consequat. At senectus ornare amet
                      suscipit. Morbi libero, nisi, diam lorem diam in feugiat risus. Augue augue egestas mauris mauris
                      fames sit at non urna. Maecenas in mi vel viverra ac accumsan eros nibh sed.
                    </p>
                  </div>
                </div>
                <img src='assets/images/about/service-img1.png' alt='Service-img1' className='img-cover img-fluid' />
              </div>
            </div>
            <div className='col-12 col-md-6 col-lg-6 p-30'>
              <div className='services_box'>
                <div className='service-content'>
                  <div className='service_wrappe'>
                    <div className='instructor'>
                      <div className='service_ic'>
                        <img src='/assets/images/svg/qualified.svg' alt='Qualified' />
                      </div>
                      <div className='service_title'>Qualified Instructar</div>
                    </div>
                    <p className='blogs-block m-0 text-start'>
                      Tempor pellentesque luctus mi porttitor at orci, tristique. Cursus quam quam vulputate enim. Enim
                      proin congue sed id aenean in quis. Cras turpis varius mauris nunc urna, vitae nisl ullamcorper.
                      Ultricies convallis elementum sed ullamcorper integer sed aliquam volutpat, blandit. Porta.
                    </p>
                  </div>
                </div>
                <img src='assets/images/about/service-img2.png' alt='Service-img2' className='img-cover img-fluid' />
              </div>
            </div>
            <div className='col-12 col-md-6 col-lg-6'>
              <div className='services_box'>
                <div className='service-content'>
                  <div className='service_wrappe'>
                    <div className='instructor'>
                      <div className='service_ic'>
                        <img src='/assets/images/svg/organic.svg' alt='organic' />
                      </div>
                      <div className='service_title'>Organic Proteins</div>
                    </div>
                    <p className='blogs-block m-0 text-start'>
                      Dui, consectetur nibh sed libero massa enim consequat pellentesque. Gravida arcu mi amet neque.
                      Aliquam pharetra euismod lectus mattis lobortis eu aliquam. Semper sit pretium sit vulputate
                      commodo, volutpat bibendum quis. Et quisque eget tellus hendrerit id morbi magna tortor enim non.
                    </p>
                  </div>
                </div>
                <img src='/assets/images/about/service-img3.png' alt='Service-img3' className='img-cover img-fluid' />
              </div>
            </div>
            <div className='col-12 col-md-6 col-lg-6'>
              <div className='services_box'>
                <div className='service-content'>
                  <div className='service_wrappe'>
                    <div className='instructor'>
                      <div className='service_ic'>
                        <img src='assets/images/svg/award.svg' alt='award program' />
                      </div>
                      <div className='service_title'>Award Programs</div>
                    </div>
                    <p className='blogs-block m-0 text-start'>
                      Vitae quisque aenean molestie risus imperdiet vulputate etiam id. Fames et lectus sagittis,
                      aliquet sed. Odio aliquam, eget iaculis vel pulvinar lacus sem mollis. Pharetra quam velit lectus
                      accumsan et euismod morbi vitae elementum. Eget massa tempor urna, placerat at blandit.
                    </p>
                  </div>
                </div>
                <img src='assets/images/about/service-img4.png' alt='Service-img4' className='img-cover img-fluid' />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default About
