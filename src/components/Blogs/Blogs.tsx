import { Swiper, SwiperSlide } from 'swiper/react'
import { listPosts } from '../../constants/blogPosts'
import { Link, Outlet } from 'react-router-dom'
import 'swiper/css'
import { Blog } from 'types/blog.type'
const Blogs = () => {
  return (
    <div className='fitzaro_blog p-10' id='news'>
      <div className='shape-img-1 animate-this'>
        <img src='/assets/images/svg/shape-post1.svg' alt='shape-post1' className='img-fluid' />
      </div>
      <div className='shape-img-2 animate-this'>
        <img src='/assets/images/svg/shape-post2.svg' alt='shape-post2' className='img-fluid' />
      </div>
      <div className='shape-img-3 animate-this'>
        <img src='/assets/images/svg/shape-post3.svg' alt='shape-post3' className='img-fluid' />
      </div>
      <div className='container'>
        <div className='blog_details mb-60 text-center'>
          <span className='sub_heading'>Our News</span>
          <h2 className='m-0'>Latest Blog Posts</h2>
        </div>
        <Swiper
          slidesPerView={1}
          spaceBetween={30}
          loop={true}
          speed={900}
          autoplay={{ delay: 3000, disableOnInteraction: false }}
          breakpoints={{ 768: { slidesPerView: 2 }, 1024: { slidesPerView: 3 } }}
          className='blog_slider'
          data-aos='zoom-in'
          data-aos-easing='linear'
          data-aos-duration={1000}
        >
          <div className='swiper-wrapper'>
            {listPosts.map((item: Blog, index: number) => (
              <SwiperSlide className='blog_post swiper-slide' key={index}>
                <div className='blog-image'>
                  <a href='blog_page.html'>
                    <img src={item.mainImage} alt='Blog-img1' className='img-fluid' />
                  </a>
                </div>
                <div className='post_data'>
                  <ul className='feature-post'>
                    <li>
                      <i className='ri-eye-line' />
                      <span>85</span>
                    </li>
                    <li className='m-0'>
                      <i className='ri-calendar-2-line' />
                      <span>{item.datePosted}</span>
                    </li>
                  </ul>
                  <Link to={`/blog/${item.slugTitle}`}>
                    <h3 className='blog_title'>{item.title}</h3>
                  </Link>
                  <div className='read-button moving-left'>
                    <Link to={`/blog/${item.slugTitle}`} className='read-more-btn'>
                      Read More
                    </Link>
                    <svg width={35} height={16} viewBox='0 0 35 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
                      <path
                        fillRule='evenodd'
                        clipRule='evenodd'
                        d='M27.5321 0.318191L34.4457 7.23182C34.87 7.65607 34.87 8.34393 34.4457 8.76818L27.5321 15.6818C27.1078 16.1061 26.42 16.1061 25.9957 15.6818C25.5715 15.2576 25.5715 14.5697 25.9957 14.1454L31.0548 9.08637H1.08637C0.486385 9.08637 0 8.59999 0 8C0 7.40001 0.486385 6.91363 1.08637 6.91363H31.0548L25.9957 1.85455C25.5715 1.4303 25.5715 0.742446 25.9957 0.318191C26.42 -0.106064 27.1078 -0.106064 27.5321 0.318191Z'
                        fill='#EA1C29'
                      />
                    </svg>
                  </div>
                </div>
              </SwiperSlide>
            ))}
          </div>
        </Swiper>
      </div>
    </div>
  )
}

export default Blogs
