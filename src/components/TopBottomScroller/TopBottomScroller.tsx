import { useEffect, useRef, useState } from 'react'

function TopBottomScroller() {
  const [showScrollTopButton, setShowScrollTopButton] = useState(false)
  let [scrollStyle, setScrollStyle] = useState({ length: 0, calcHeight: 0 })
  let pathRef = useRef<SVGPathElement>()
  useEffect(() => {
    window.addEventListener('scroll', () => {
      if (window.scrollY > 300) {
        setShowScrollTopButton(true)
      } else {
        setShowScrollTopButton(false)
      }
    })
    return () => {
      window.removeEventListener('scroll', () => {
        if (window.scrollY > 300) {
          setShowScrollTopButton(true)
        } else {
          setShowScrollTopButton(false)
        }
      })
    }
  })

  const scrollTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    })
  }
  function round(value: number | undefined, decimals: number) {
    return Number(Math.round(Number(value + 'e' + decimals)) + 'e-' + decimals)
  }

  const scrollPercentage = () => {
    let length = round(pathRef.current?.getTotalLength(), 3)
    let calcHeight = document.documentElement.scrollHeight - document.documentElement.clientHeight

    setScrollStyle({ calcHeight, length })
  }

  window.onscroll = scrollPercentage
  return (
    <div>
      {showScrollTopButton && (
        <div
          className='scroll-top active'
          style={{
            strokeDasharray: scrollStyle.length ? scrollStyle.length + ' ' + scrollStyle.length : '',
            transition: 'stroke-dashoffset 10ms linear',
            strokeDashoffset: scrollStyle.length
              ? scrollStyle.length - (document.documentElement.scrollTop * scrollStyle.length) / scrollStyle.calcHeight
              : ''
          }}
          id='progress'
          onClick={() => scrollTop()}
        >
          <svg className='border-circle svg-inner' width='100%' height='100%' viewBox='-1 -1 102 102'>
            <path
              id='circle-id'
              ref={(ref: SVGPathElement) => (pathRef.current = ref)}
              className='svg'
              d='M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98'
            />
          </svg>
        </div>
      )}
    </div>
  )
}

export default TopBottomScroller
