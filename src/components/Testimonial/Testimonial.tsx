import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
function Testimonial() {
  return (
    <div className='testimonial-section p-10' id='testimonial'>
      <div className='container'>
        <div className='testimonial_detail mb-60 text-center'>
          <span className='sub_heading'>Testimonial</span>
          <h2 className='m-0'>What Our Clients Says</h2>
        </div>
        <Swiper
          slidesPerView={1}
          spaceBetween={30}
          loop={true}
          autoplay={{ delay: 5000, disableOnInteraction: false }}
          speed={900}
          breakpoints={{ 768: { slidesPerView: 2 }, 1024: { slidesPerView: 2 } }}
          className='testimonial_slider'
          data-aos='zoom-in-down'
          data-aos-easing='linear'
          data-aos-duration={1000}
        >
          <div className='swiper-wrapper'>
            <SwiperSlide className='client_review swiper-slide'>
              <div className='review_detail'>
                <p className='m-0'>
                  Nibh volutpat rhoncus tortor ac. Posuere mattis orci, scelerisque volutpat dignissim nullam nascetur
                  feugiat tortor. Potenti viverra a sed in felis. Tincidunt habitant et scelerisque sit at sit risus
                  neque tincidunt. A tempor malesuada eget enim, eleifend. Tincidunt feugiat risus.
                </p>
              </div>
              <div className='client_profile'>
                <img src='assets/images/testimonial/profile-img1.png' alt='profilr-img1' className='img-fluid' />
                <div className='testimonial_data'>
                  <h3 className='m-0'>Eliana Dliya</h3>
                  <p>CEO</p>
                  <div className='star-trating'>
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                  </div>
                </div>
              </div>
            </SwiperSlide>
            <div className='client_review swiper-slide'>
              <div className='review_detail'>
                <p className='m-0'>
                  Nibh volutpat rhoncus tortor ac. Posuere mattis orci, scelerisque volutpat dignissim nullam nascetur
                  feugiat tortor. Potenti viverra a sed in felis. Tincidunt habitant et scelerisque sit at sit risus
                  neque tincidunt. A tempor malesuada eget enim, eleifend. Tincidunt feugiat risus.
                </p>
              </div>
              <div className='client_profile'>
                <img src='assets/images/testimonial/profile-img2.png' alt='profilr-img12' className='img-fluid' />
                <div className='testimonial_data'>
                  <h3 className='m-0'>Jardon Smith</h3>
                  <p>CEO</p>
                  <div className='star-trating'>
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                  </div>
                </div>
              </div>
            </div>
            <SwiperSlide className='client_review swiper-slide'>
              <div className='review_detail'>
                <p className='m-0'>
                  Nibh volutpat rhoncus tortor ac. Posuere mattis orci, scelerisque volutpat dignissim nullam nascetur
                  feugiat tortor. Potenti viverra a sed in felis. Tincidunt habitant et scelerisque sit at sit risus
                  neque tincidunt. A tempor malesuada eget enim, eleifend. Tincidunt feugiat risus.
                </p>
              </div>
              <div className='client_profile'>
                <img src='assets/images/testimonial/profile-img1.png' alt='profilr-img1' className='img-fluid' />
                <div className='testimonial_data'>
                  <h3 className='m-0'>Eliana Dliya</h3>
                  <p>CEO</p>
                  <div className='star-trating'>
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                  </div>
                </div>
              </div>
            </SwiperSlide>
            <SwiperSlide className='client_review swiper-slide'>
              <div className='review_detail'>
                <p className='m-0'>
                  Nibh volutpat rhoncus tortor ac. Posuere mattis orci, scelerisque volutpat dignissim nullam nascetur
                  feugiat tortor. Potenti viverra a sed in felis. Tincidunt habitant et scelerisque sit at sit risus
                  neque tincidunt. A tempor malesuada eget enim, eleifend. Tincidunt feugiat risus.
                </p>
              </div>
              <div className='client_profile'>
                <img src='assets/images/testimonial/profile-img2.png' alt='profilr-img12' className='img-fluid' />
                <div className='testimonial_data'>
                  <h3 className='m-0'>Jardon Smith</h3>
                  <p>CEO</p>
                  <div className='star-trating'>
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                    <i className='ri-star-fill' />
                  </div>
                </div>
              </div>
            </SwiperSlide>
          </div>
        </Swiper>
      </div>
    </div>
  )
}

export default Testimonial
