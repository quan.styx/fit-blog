import './floatingbuttonOut.css'
import { getAuth, signOut } from 'firebase/auth'
import { useAppDispatch } from '../../store'
import { logout } from '../../pages/authSlice'
interface LogoutType {
  name: string
}
export default function FloatingLogoutnButton(props: LogoutType) {
  const dispatch = useAppDispatch()
  const logoutHandler = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    const auth = getAuth()
    signOut(auth)
      .then(() => {
        // Sign-out successful.
        dispatch(logout)
        localStorage.removeItem('token')
        sessionStorage.removeItem('token')
        sessionStorage.removeItem('userId')
        sessionStorage.removeItem('userName')
        sessionStorage.removeItem('email')
      })
      .catch((error) => {
        // An error happened.
        console.log(error)
      })
  }

  return (
    <div>
      <a className='float-sgo-btn active' onClick={(e) => logoutHandler(e)}>
        WELCOME, {props.name}
      </a>
    </div>
  )
}
