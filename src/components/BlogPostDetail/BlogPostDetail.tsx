import { Blog as blogType } from '../../types/blog.type'
interface BlogPostDetailType {
  blog: blogType
}
export default function BlogPostDetail(props: BlogPostDetailType) {
  return (
    <>
      {/* start blog-post detail section */}
      <div className='about-section post_detail p-10' id='about'>
        <h2 className='d-none'>Hidden</h2>
        <div className='container'>
          <div className='row'>
            <div className='col-12 col-lg-6 col-md-6 video_wrap' data-aos='fade-right' data-aos-duration={1000}>
              <div className='bg_image'>
                <div className='line-shapes'>
                  <div className='line-1' />
                  <div className='line-2' />
                  <div className='line-3' />
                  <div className='line-4' />
                </div>
                <div className='right-arrow-grp'>
                  <div className='arrowSliding'>
                    <div className='arrow' />
                  </div>
                  <div className='arrowSliding delay1'>
                    <div className='arrow' />
                  </div>
                  <div className='arrowSliding delay2'>
                    <div className='arrow' />
                  </div>
                  <div className='arrowSliding delay3'>
                    <div className='arrow' />
                  </div>
                  <div className='arrowSliding delay4'>
                    <div className='arrow' />
                  </div>
                  <div className='arrowSliding delay5'>
                    <div className='arrow' />
                  </div>
                </div>
                <div className='top-arrow-grp'>
                  <div className='slide-top'>
                    <div className='arrow' />
                  </div>
                  <div className='slide-top delaytime1'>
                    <div className='arrow' />
                  </div>
                  <div className='slide-top delaytime2'>
                    <div className='arrow' />
                  </div>
                  <div className='slide-top delaytime3'>
                    <div className='arrow' />
                  </div>
                  <div className='slide-top delaytime4'>
                    <div className='arrow' />
                  </div>
                  <div className='slide-top delaytime5'>
                    <div className='arrow' />
                  </div>
                </div>
                <img src={props.blog.mainImage} alt='Mission-video' className='video-img img-fluid' />
              </div>
            </div>
            <div className='col-12 col-lg-6 col-md-6'>
              <div className='video_info'>
                <div className='post_view d-flex'>
                  <div className='view-person d-flex'>
                    <i className='ri-eye-line' />
                    <p className='m-0'>85</p>
                  </div>
                  <div className='post_date d-flex'>
                    <i className='ri-calendar-2-line' />
                    <p className='m-0'>{props.blog.datePosted}</p>
                  </div>
                </div>
                <p className='post_content m-0'>{props.blog.subPostData}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='sub-post-data'>
        <div className='container'>
          <h2 className='m-0' data-aos='fade-down' data-aos-easing='linear' data-aos-duration={1000}>
            {props.blog.subPostTitle}
          </h2>
          <p className='weight-formula m-0'>{props.blog.subPostData}</p>
          <div className='check_points d-flex'>
            <div className='box_detail'>
              {props.blog.subPostList1.map((item: string) => (
                <div className='list_icon'>
                  <i className='ri-check-line' />
                  <p className='check_detail m-0'>{item}</p>
                </div>
              ))}
            </div>
            <div className='box_detail'>
              {props.blog.subPostList2.map((item: string) => (
                <div className='list_icon'>
                  <i className='ri-check-line' />
                  <p className='check_detail m-0'>{item}</p>
                </div>
              ))}
            </div>
          </div>
          <div
            className='row fizaro_blog_images'
            data-aos='zoom-in-up'
            data-aos-easing='linear'
            data-aos-duration={1000}
          >
            {props.blog.images.map((item: string) => (
              <div className='col-12 col-md-4 col-lg-4'>
                <div className='blog_images'>
                  <img src={`/${item}`} alt='blog_post2' className='img-fluid' />
                </div>
              </div>
            ))}
          </div>
          <p className='inn-sub-detail m-0 p-8'>{props.blog.subDetail}</p>
          <div className='col-12 p-8' data-aos='fade-up' data-aos-easing='linear' data-aos-duration={1000}>
            <div className='head_review'>{props.blog.quote}</div>
            <p className='review_name'>{props.blog.author}</p>
          </div>
          <div className='post_social_info'>
            <div className='social-ic-listed text-center'>
              <a href='https://www.facebook.com/' className='facebook-ic'>
                <i className='ri-facebook-fill icons' />
              </a>
              <a href='https://twitter.com/' className='twitter-ic'>
                <i className='ri-twitter-fill icons' />
              </a>
              <a href='https://www.instagram.com/' className='instagram-ic'>
                <i className='ri-instagram-line icons' />
              </a>
              <a href='https://web.whatsapp.com/' className='whatsapp-ic'>
                <i className='ri-whatsapp-fill' />
              </a>
            </div>
          </div>
        </div>
      </div>
      {/* end blog-post detail section */}
    </>
  )
}
