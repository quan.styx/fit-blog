import { useForm } from 'react-hook-form'

type FormValues = {
  fullName: string
  email: string
  phoneNumber: number
  message: string
}

function Appointment() {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors }
  } = useForm<FormValues>()
  const onSubmit = (data: any) => {}
  return (
    <div className='appointment_section reveal p-10' id='appointment'>
      <div className='parallax-overlay' />
      <div className='container'>
        <div className='row'>
          <div className='col-12 col-lg-6 col-md-6'>
            <div className='static_content'>
              <span className='sub_heading'>Our Statics</span>
              <h2 className='main_heading m-0 text-white'>Every day is a chance to become better.</h2>
              <p className='mb-60 text-white'>
                Quis bibendum mi auctor lectus arcu, sapien cursus pharetra. Facilisis ut dignissim volutpat a sit quis
                pharetra habitasse nec. Ut faucibus habitant magna sed morbi lectus suscipit in enim. Nulla at
                consectetur adipiscing id auctor tellus sit pretium eu. Molestie consectetur donec nec non tincidunt
                posuere lorem et et. Quam rutrum pharetra quis euismod non. Ultrices magna enim, in porttitor integer.
              </p>
              <div className='skill_bar'>
                <div className='progress-counter'>
                  <p className='progress-title text-start'>Client Satisfaction</p>
                  <div className='progress-value' style={{ left: '90%' }}>
                    <span className='count coundown-number text-white' data-number={90}>
                      90
                    </span>
                    %
                  </div>
                  <div className='skil-progressbar'>
                    <span style={{ width: '90%' }} />
                  </div>
                </div>
                <div className='progress-counter'>
                  <p className='progress-title text-start'>Support Customer</p>
                  <div className='progress-value' style={{ left: '80%' }}>
                    <span className='count coundown-number text-white' data-number={80}>
                      80
                    </span>
                    %
                  </div>
                  <div className='skil-progressbar'>
                    <span style={{ width: '80%' }} />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='col-12 col-lg-6 col-md-6'>
            <div className='appointment_form' data-aos='fade-left' data-aos-duration={1000}>
              <div className='get_title'>
                <h2 className='m-0 mb-4 text-center text-white'>Get Appointment</h2>
              </div>
              <form onSubmit={handleSubmit(onSubmit)} className='form_start'>
                <div className='row'>
                  <div className='col-12'>
                    <div className='input_area'>
                      <input type='text' name='Full Name' className='inputt-text' placeholder='Full Name' />
                    </div>
                  </div>
                  <div className='col-12'>
                    <div className='input_area'>
                      <input type='text' name='Phone Number' className='inputt-text' placeholder='Phone Number' />
                    </div>
                  </div>
                  <div className='col-12'>
                    <div className='input_area'>
                      <input type='email' name='Email Address' className='inputt-text' placeholder='Email Address' />
                    </div>
                  </div>
                  <div className='col-12'>
                    <div className='input_area'>
                      <textarea className='inputt-text' placeholder='Message...' defaultValue={''} />
                    </div>
                  </div>
                  <div className='col-12'>
                    <div className='input_area mb-0'>
                      <button type='submit' className='button fitzaro_btn'>
                        Submit Now
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Appointment
