function Video() {
  return (
    <div className='fitzaro_video p-10' id='mission'>
      <div className='container'>
        <div className='row'>
          <div className='col-12 col-lg-6 col-md-6'>
            <div className='video_info col-lg-10 col-md-12'>
              <span className='sub_heading'>Our Mission</span>
              <h2 className='main_heading m-0 text-white'>Get In. Get Fit. Get on with Life.</h2>
              <p className='m-0 p-8'>
                Nibh volutpat rhoncus tortor ac. Posuere mattis orci, scelerisque volutpat dignissim nullam nascetur
                feugiat tortor. Potenti viverra a sed in felis. Tincidunt habitant et scelerisque sit at sit risus neque
                tincidunt. A tempor malesuada eget enim, eleifend. Tincidunt feugiat risus, euismod et nullam. Interdum
                lectus hendrerit nibh cursus diam viverra facilisis in.
              </p>
              <div className='video-play d-flex'>
                <button
                  //href='javascript:void(0);'
                  className='video_button'
                  data-bs-toggle='modal'
                  data-bs-target='#exampleModal'
                >
                  <i className='ri-play-fill' />
                </button>
                <p className='m-0'>Watch Now</p>
              </div>
            </div>
          </div>
          <div className='col-12 col-lg-6 col-md-6 video_wrap' data-aos='fade-left' data-aos-duration={1000}>
            <div className='bg_image'>
              <div className='line-shapes'>
                <div className='line-1' />
                <div className='line-2' />
                <div className='line-3' />
                <div className='line-4' />
              </div>
              <div className='right-arrow-grp'>
                <div className='arrowSliding'>
                  <div className='arrow' />
                </div>
                <div className='arrowSliding delay1'>
                  <div className='arrow' />
                </div>
                <div className='arrowSliding delay2'>
                  <div className='arrow' />
                </div>
                <div className='arrowSliding delay3'>
                  <div className='arrow' />
                </div>
                <div className='arrowSliding delay4'>
                  <div className='arrow' />
                </div>
                <div className='arrowSliding delay5'>
                  <div className='arrow' />
                </div>
              </div>
              <div className='top-arrow-grp'>
                <div className='slide-top'>
                  <div className='arrow' />
                </div>
                <div className='slide-top delaytime1'>
                  <div className='arrow' />
                </div>
                <div className='slide-top delaytime2'>
                  <div className='arrow' />
                </div>
                <div className='slide-top delaytime3'>
                  <div className='arrow' />
                </div>
                <div className='slide-top delaytime4'>
                  <div className='arrow' />
                </div>
                <div className='slide-top delaytime5'>
                  <div className='arrow' />
                </div>
              </div>
              <img src='assets/images/mission/video-img.png' alt='Mission-video' className='video-img img-fluid' />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Video
