import './floatingbutton.css'
import { signInWithGoogle } from '../../services/firebase'
import { GoogleAuthProvider } from 'firebase/auth'
import { useAppDispatch } from '../../store'
import { loginWithGoogleFirebase } from '../../pages/authSlice'
export default function FloatingLoginButton() {
  const dispatch = useAppDispatch()
  const loginHandler = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    signInWithGoogle()
      .then((result) => {
        // Lấy ra Google Access Token. Ta có thể sử dụng Token này để truy cập vào API.
        const credential = GoogleAuthProvider.credentialFromResult(result)
        const token = credential?.accessToken
        // Lấy ra thông tin của user
        const user = result.user
        //console.log(user)
        // ... Lưu thông tin vào trong 1 global State Vd: React Context hoặc Redux
        if (user.displayName && user.email && token !== undefined && user.uid) {
          dispatch(
            loginWithGoogleFirebase({ userName: user.displayName, email: user.email, token: token, userId: user.uid })
          )
          localStorage.setItem('token', token)
          sessionStorage.setItem('token', token)
          sessionStorage.setItem('userId', user.uid)
          sessionStorage.setItem('userName', user.displayName)
          sessionStorage.setItem('email', user.email)
        }
      })
      .catch((error) => {
        // Kiểm tra lỗi
        const errorCode = error.code
        const errorMessage = error.message
        // The email of the user's account used.
        const email = error.customData.email
        // The AuthCredential type that was used.
        const credential = GoogleAuthProvider.credentialFromError(error)
        // ...
        console.log(error.message)
      })
  }

  return (
    <div>
      <a className='float-sgn-btn active' onClick={(e) => loginHandler(e)}>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        SIGN IN
      </a>
    </div>
  )
}
