import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
export default function Slider() {
  return (
    <>
      {/* start first section */}
      <div className='first_section fitzaro_slider social-triangle' id='home'>
        <Swiper
          effect='coverflow'
          autoplay={{ delay: 5000, disableOnInteraction: false }}
          loop={true}
          centeredSlides={true}
          speed={1500}
          simulateTouch={true}
          parallax={true}
          watchSlidesProgress={true}
        >
          <div className='swiper-wrapper'>
            <SwiperSlide>
              <div className='slide-inner'>
                <div
                  className='slide-bg-image'
                  style={{ backgroundImage: 'url(assets/images/slider/slider_img1.jpg)' }}
                />
                <div className='parallax-overlay' />
                <div className='container'>
                  <div className='banner_content slide_inn_content col-12 col-lg-9 col-md-10'>
                    <span className='gym-head'>The Best Fitness And Gym Studio</span>
                    <h1 className='main_tiltle m-0'>Build Your Perfect Body And Healthy Growth</h1>
                    <p>
                      Risus ultricies feugiat iaculis est. Eget blandit fringilla in vivamus urna eu. Augue et rhoncus
                      gravida in velit at a amet. Dolor lacinia tristique morbi sed turpis turpis tellus cras in.
                    </p>
                    <a className='button fitzaro_btn' href='#appointment'>
                      Book Appointment
                    </a>
                  </div>
                  <a href='#about' className='scroll-down'>
                    <span className='scroll-arrow arrow1' />
                    <span className='scroll-arrow arrow2' />
                    <span className='scroll-arrow arrow3' />
                  </a>
                </div>
              </div>
            </SwiperSlide>
            <SwiperSlide>
              <div className='slide-inner'>
                <div
                  className='slide-bg-image'
                  style={{ backgroundImage: 'url(assets/images/slider/slider_img2.jpg)' }}
                />
                <div className='parallax-overlay' />
                <div className='container'>
                  <div className='banner_content slide_inn_content col-12 col-lg-9 col-md-10'>
                    <span className='gym-head'>The Best Fitness And Gym Studio</span>
                    <h1 className='main_tiltle m-0'>Build Your Perfect Body And Healthy Growth</h1>
                    <p>
                      Risus ultricies feugiat iaculis est. Eget blandit fringilla in vivamus urna eu. Augue et rhoncus
                      gravida in velit at a amet. Dolor lacinia tristique morbi sed turpis turpis tellus cras in.
                    </p>
                    <a className='button fitzaro_btn' href='#appointment'>
                      Book Appointment
                    </a>
                  </div>
                  <a href='#about' className='scroll-down'>
                    <span className='scroll-arrow arrow1' />
                    <span className='scroll-arrow arrow2' />
                    <span className='scroll-arrow arrow3' />
                  </a>
                </div>
              </div>
            </SwiperSlide>
            <Swiper>
              <div className='slide-inner'>
                <div
                  className='slide-bg-image'
                  style={{ backgroundImage: 'url(assets/images/slider/slider_img3.jpg)' }}
                />
                <div className='parallax-overlay' />
                <div className='container'>
                  <div className='banner_content slide_inn_content col-12 col-lg-9 col-md-10'>
                    <span className='gym-head'>The Best Fitness And Gym Studio</span>
                    <h1 className='main_tiltle m-0'>Build Your Perfect Body And Healthy Growth</h1>
                    <p>
                      Risus ultricies feugiat iaculis est. Eget blandit fringilla in vivamus urna eu. Augue et rhoncus
                      gravida in velit at a amet. Dolor lacinia tristique morbi sed turpis turpis tellus cras in.
                    </p>
                    <a className='button fitzaro_btn' href='#appointment'>
                      Book Appointment
                    </a>
                  </div>
                  <a href='#about' className='scroll-down'>
                    <span className='scroll-arrow arrow1' />
                    <span className='scroll-arrow arrow2' />
                    <span className='scroll-arrow arrow3' />
                  </a>
                </div>
              </div>
            </Swiper>
          </div>
        </Swiper>
        <div className='slide_social_ic' data-aos='fade-up-left' data-aos-duration={1000}>
          <div className='triangle-1' />
          <div className='banner_social'>
            <a href='https://www.facebook.com/' target='_blank' rel='noreferrer'>
              FB
            </a>
            <a href='https://www.instagram.com/' target='_blank' rel='noreferrer'>
              IG
            </a>
            <a href='https://www.youtube.com/' target='_blank' rel='noreferrer'>
              YT
            </a>
          </div>
        </div>
      </div>
      {/* end first section */}
    </>
  )
}
