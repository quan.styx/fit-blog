function PlayVideo() {
  return (
    <div className='modal fade video_modal' id='exampleModal' tabIndex={-1} aria-hidden='true'>
      <div className='modal-dialog modal-dialog-centered'>
        <div className='modal-content'>
          <div className='modal-header'>
            <button type='button' className='btn-close' data-bs-dismiss='modal' aria-label='Close' />
          </div>
          <div className='modal-body'>
            <video width={727} height={410} controls={true} autoPlay={false}>
              <source src='assets/video/gym_video.mp4' type='video/mp4' />
            </video>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PlayVideo
