import './loader.css'
export default function Preloader() {
  return (
    <div className='preloader'>
      <div className='loader'>
        <div className='loader-inner'></div>
        <div className='loader-inner'></div>
      </div>
    </div>
  )
}
