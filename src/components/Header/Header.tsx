import { useEffect, useState } from 'react'
interface HeaderType {
  for: string
  position: string
}
const HomeContent = [
  {
    itemName: 'Home',
    itemRoute: '#home',
    activeClass: 'active'
  },
  {
    itemName: 'About',
    itemRoute: '#about',
    activeClass: ''
  },
  {
    itemName: 'Mission',
    itemRoute: '#mission',
    activeClass: ''
  },
  {
    itemName: 'Services',
    itemRoute: '#service',
    activeClass: ''
  },
  {
    itemName: 'Classes',
    itemRoute: '#classes',
    activeClass: ''
  },
  {
    itemName: 'Testimonial',
    itemRoute: '#testimonial',
    activeClass: ''
  },
  {
    itemName: 'Pricing',
    itemRoute: '#price',
    activeClass: ''
  },
  {
    itemName: 'Trainer',
    itemRoute: '#trainer',
    activeClass: ''
  },
  {
    itemName: 'News',
    itemRoute: '#news',
    activeClass: ''
  },
  {
    itemName: 'Contact Us',
    itemRoute: '#contact',
    activeClass: ''
  }
]

const BlogContent = [
  {
    itemName: 'Home',
    itemRoute: '#home',
    activeClass: 'active'
  },
  {
    itemName: 'Blog',
    itemRoute: '#blog',
    activeClass: ''
  }
]
export default function Header(props: HeaderType) {
  let [sidebarToggle, setSidebarToggle] = useState(false)
  let [navbarToggle, setNavbarToggle] = useState(false)
  let [position, SetPosition] = useState(window.scrollY)
  useEffect(() => {
    window.addEventListener('scroll', scrollHandler)
    return () => {
      window.removeEventListener('scroll', scrollHandler)
    }
  })
  const scrollHandler = (e: any) => {
    //Sticky handler
    SetPosition(window.scrollY)

    //ActiveNavbar handler
    const currentScrollPos = document.documentElement.scrollTop

    document.querySelectorAll('.navbar_collapse ul li a').forEach((item) => {
      let section = document.querySelector(`${item.getAttribute('href')}`)
      let refElement = section?.getBoundingClientRect()
      if (refElement) {
        if (
          refElement.top + window.scrollY <= currentScrollPos + 1 &&
          refElement.top + window.scrollY + refElement.height > currentScrollPos + 1
        ) {
          item.classList.remove('active')
          item.classList.add('active')
        } else {
          item.classList.remove('active')
        }
      }
    })
  }

  return (
    <div className='header' id='header'>
      <div className={`navbar-header ${position >= 40 ? 'sticky' : ''} `}>
        <nav className='navbar_expand'>
          <div className='navbar_main'>
            <div className='navbar_panel'>
              {/*Navbar Toggle*/}
              <button
                className={`navbar-toggle toggle-menu ${navbarToggle ? 'static' : ''}`}
                onClick={() => setNavbarToggle(!navbarToggle)}
              >
                <span />
              </button>
              {/*Navbar Logo*/}
              <div className='navbar_brand'>
                {/*Logo*/}
                <a className='fitzaro_logo' href='index.html'>
                  <img src='/assets/images/logo/logo.png' alt='Header Logo img' className='logo_dark img-fluid' />
                </a>
              </div>
            </div>
            <div className={`navbar_collapse ${navbarToggle ? 'static' : ''}`}>
              <nav>
                <ul className='navbar_nav me-auto mb-lg-0 mb-2'>
                  {(function () {
                    switch (props.for) {
                      case 'home':
                        return HomeContent.map((item, index) => (
                          <li className='nav-item' key={index}>
                            <a className={`nav-link ${item.activeClass}`} href={item.itemRoute} key={index}>
                              {item.itemName}
                            </a>
                          </li>
                        ))

                      case 'blog':
                        return BlogContent.map((item, index) => (
                          <li className='nav-item' key={index}>
                            <a className={`nav-link ${item.activeClass}`} href={item.itemRoute} key={index}>
                              {item.itemName}
                            </a>
                          </li>
                        ))
                      default:
                        return HomeContent.map((item, index) => (
                          <li className='nav-item' key={index}>
                            <a className={`nav-link ${item.activeClass}`} href={item.itemRoute}>
                              {item.itemName}
                            </a>
                          </li>
                        ))
                    }
                  })()}
                </ul>
              </nav>
            </div>
            <button
              className={`sidebar-toggle fixed-menu ${sidebarToggle ? 'active' : ' '}`}
              onClick={() => setSidebarToggle(!sidebarToggle)}
            >
              <span />
            </button>
          </div>
        </nav>
        <div
          className={`sidebar-menu ? ${sidebarToggle ? 'active' : ' '}`}
          onClick={() => setSidebarToggle(!sidebarToggle)}
        >
          <div className='side_social_info'>
            <img src='assets/images/svg/watch.svg' alt='clock' className='img-fluid' />
            <div className='time-schedule'>
              <div className='weekly-day'>Mon - Fri</div>
              <p className='m-0'>7 AM - 10 PM</p>
            </div>
            <div className='time-schedule'>
              <div className='weekly-day'>Sat - Sun</div>
              <p className='m-0'>7 AM - 2 PM</p>
            </div>
            <ul className='social_list'>
              <li>
                <a href='tel:+012345678899'>+01 234 567 8899</a>
              </li>
              <li className='mb-5'>
                <a href='mailto:contact@fitzaro.com'>contact@fitzaro.com</a>
              </li>
              <li>
                <p className='m-0'>121 Manila St. Brookly, CA</p>
              </li>
            </ul>
          </div>
          <div className='side_panel'>
            <ul className='list-social-ic'>
              <li>
                <a className='icon ic-facebook' href='https://www.facebook.com/'>
                  <i className='ri-facebook-fill' />
                </a>
              </li>
              <li>
                <a className='icon ic-twitter' href='https://twitter.com/'>
                  <i className='ri-twitter-fill' />
                </a>
              </li>
              <li>
                <a className='icon ic-instgram' href='https://www.instagram.com/'>
                  <i className='ri-instagram-line' />
                </a>
              </li>
              <li>
                <a className='icon ic-youtube' href='https://www.youtube.com/'>
                  <i className='ri-youtube-fill' />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  )
}
