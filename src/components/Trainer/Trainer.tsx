import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
function Trainer() {
  return (
    <div className='best-trainer p-10' id='trainer'>
      <div className='img-shape1'>
        <img src='assets/images/trainer/gym-img.png' alt='gym' className='img-fluid' />
      </div>
      <div className='img-shape2'>
        <img src='assets/images/trainer/muscle-img.png' alt='muscle-img' className='img-fluid' />
      </div>
      <div className='container'>
        <div className='trainer-data mb-60 text-center'>
          <span className='sub_heading'>Best Trainer</span>
          <h2 className='m-0'>Our Professional Trainer</h2>
          <h3 className='d-none'>Hidden</h3>
        </div>
        <Swiper
          slidesPerView={1}
          spaceBetween={30}
          loop={true}
          autoplay={{ delay: 3000, disableOnInteraction: false }}
          speed={900}
          breakpoints={{ 768: { slidesPerView: 2 }, 1024: { slidesPerView: 3 } }}
          className='trainer_slider'
          data-aos='zoom-in'
          data-aos-easing='linear'
          data-aos-duration={1000}
        >
          <div className='swiper-wrapper'>
            <SwiperSlide className='team-box swiper-slide text-center'>
              <div className='border-shape'>
                <img src='assets/images/trainer/trainer-img1.png' alt='Trainer image1' className='img-fluid' />
              </div>
              <div className='box-inner'>
                <h4 className='d-none'>Hidden</h4>
                <h5 className='trainer-name m-0'>Daliya Anthony</h5>
                <p className='designation m-0'>Trainer</p>
                <p className='trainer-detail m-0'>
                  Purus vestibulum eros, laoreet eget. At auctor vulputate ut turpis faucibus urna aliquam tincidunt
                  tellus. Dolor eu interdum consequat morbi. Elit sit pellentesque adipiscing.
                </p>
              </div>
              <ul className='team-social'>
                <li className='ic-list'>
                  <a href='https://www.facebook.com/'>
                    <i className='ri-facebook-fill' />
                  </a>
                </li>
                <li className='ic-list'>
                  <a href='https://twitter.com/'>
                    <i className='ri-twitter-fill' />
                  </a>
                </li>
                <li className='ic-list'>
                  <a href='https://www.instagram.com/'>
                    <i className='ri-instagram-fill' />
                  </a>
                </li>
              </ul>
            </SwiperSlide>
            <SwiperSlide className='team-box swiper-slide text-center'>
              <div className='border-shape'>
                <img src='assets/images/trainer/trainer-img2.png' alt='Trainer image2' className='img-fluid' />
              </div>
              <div className='box-inner'>
                <h5 className='trainer-name m-0'>John Hardson</h5>
                <p className='designation m-0'>Trainer</p>
                <p className='trainer-detail m-0'>
                  Vel rhoncus viverra justo faucibus. Diam aliquet cursus laoreet lobortis aliquam nunc tellus. Enim
                  ultrices turpis dolor quam gravida. Diam nisl lectus donec tempor. Quis.
                </p>
              </div>
              <ul className='team-social'>
                <li className='ic-list'>
                  <a href='https://www.facebook.com/'>
                    <i className='ri-facebook-fill' />
                  </a>
                </li>
                <li className='ic-list'>
                  <a href='https://twitter.com/'>
                    <i className='ri-twitter-fill' />
                  </a>
                </li>
                <li className='ic-list'>
                  <a href='https://www.instagram.com/'>
                    <i className='ri-instagram-fill' />
                  </a>
                </li>
              </ul>
            </SwiperSlide>
            <SwiperSlide className='team-box swiper-slide text-center'>
              <div className='border-shape'>
                <img src='assets/images/trainer/trainer-img3.png' alt='Trainer image3' className='img-fluid' />
              </div>
              <div className='box-inner'>
                <h5 className='trainer-name m-0'>Ketty Smith</h5>
                <p className='designation m-0'>Trainer</p>
                <p className='trainer-detail m-0'>
                  Consectetur morbi sed gravida diam. Sed in felis a pellentesque vel molestie cras vitae. Feugiat elit
                  turpis sed sed viverra. Ante amet praesent aliquet neque, enim orci, felis. Eros.
                </p>
              </div>
              <ul className='team-social'>
                <li className='ic-list'>
                  <a href='https://www.facebook.com/'>
                    <i className='ri-facebook-fill' />
                  </a>
                </li>
                <li className='ic-list'>
                  <a href='https://twitter.com/'>
                    <i className='ri-twitter-fill' />
                  </a>
                </li>
                <li className='ic-list'>
                  <a href='https://www.instagram.com/'>
                    <i className='ri-instagram-fill' />
                  </a>
                </li>
              </ul>
            </SwiperSlide>
            <SwiperSlide className='team-box swiper-slide text-center'>
              <div className='border-shape'>
                <img src='assets/images/trainer/trainer-img4.png' alt='Trainer image4' className='img-fluid' />
              </div>
              <div className='box-inner'>
                <h5 className='trainer-name m-0'>Lipsa Smith</h5>
                <p className='designation m-0'>Trainer</p>
                <p className='trainer-detail m-0'>
                  Consectetur morbi sed gravida diam. Sed in felis a pellentesque vel molestie cras vitae. Feugiat elit
                  turpis sed sed viverra. Ante amet praesent aliquet neque, enim orci, felis. Eros.
                </p>
              </div>
              <ul className='team-social'>
                <li className='ic-list'>
                  <a href='https://www.facebook.com/'>
                    <i className='ri-facebook-fill' />
                  </a>
                </li>
                <li className='ic-list'>
                  <a href='https://twitter.com/'>
                    <i className='ri-twitter-fill' />
                  </a>
                </li>
                <li className='ic-list'>
                  <a href='https://www.instagram.com/'>
                    <i className='ri-instagram-fill' />
                  </a>
                </li>
              </ul>
            </SwiperSlide>
            <SwiperSlide className='team-box swiper-slide text-center'>
              <div className='border-shape'>
                <img src='assets/images/trainer/trainer-img5.png' alt='Trainer image5' className='img-fluid' />
              </div>
              <div className='box-inner'>
                <h5 className='trainer-name m-0'>Ali Shah</h5>
                <p className='designation m-0'>Trainer</p>
                <p className='trainer-detail m-0'>
                  Consectetur morbi sed gravida diam. Sed in felis a pellentesque vel molestie cras vitae. Feugiat elit
                  turpis sed sed viverra. Ante amet praesent aliquet neque, enim orci, felis. Eros.
                </p>
              </div>
              <ul className='team-social'>
                <li className='ic-list'>
                  <a href='https://www.facebook.com/'>
                    <i className='ri-facebook-fill' />
                  </a>
                </li>
                <li className='ic-list'>
                  <a href='https://twitter.com/'>
                    <i className='ri-twitter-fill' />
                  </a>
                </li>
                <li className='ic-list'>
                  <a href='https://www.instagram.com/'>
                    <i className='ri-instagram-fill' />
                  </a>
                </li>
              </ul>
            </SwiperSlide>
          </div>
        </Swiper>
      </div>
    </div>
  )
}

export default Trainer
