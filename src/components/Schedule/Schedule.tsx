function Classes() {
  return (
    <div className='schedule_part p-10' id='classes'>
      <div className='container'>
        <div className='schedule_start mb-60 text-center'>
          <span className='sub_heading'>Our Schedule</span>
          <h2 className='m-0'>Check Our Classes Schedule</h2>
        </div>
        <div className='row'>
          <div className='col-lg-12'>
            <div className='gym_timetable' data-aos='zoom-in-up' data-aos-easing='linear' data-aos-duration={1000}>
              <div className='timetable-area table-responsive'>
                <table className='table'>
                  <thead>
                    <tr>
                      <th>Monday</th>
                      <th>Tuesday</th>
                      <th>Wednesday</th>
                      <th>Thursday</th>
                      <th>Friday</th>
                      <th>Saturday</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Weight Lifting</h3>
                          <span>John Cina</span>
                          <div className='schedule_time'>
                            <h4>8 AM - 10 AM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/weight-1.png'
                          alt='weight-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Aerobics</h3>
                          <span>Tony Kiano</span>
                          <div className='schedule_time'>
                            <h4>8 AM - 10 AM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/aerobics-1.png'
                          alt='Aerobics-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Boxing</h3>
                          <span>Steven Smith</span>
                          <div className='schedule_time'>
                            <h4>8 AM - 10 AM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/boxing-1.png'
                          alt='boxing-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Aerobics</h3>
                          <span>Tony Kiano</span>
                          <div className='schedule_time'>
                            <h4>8 AM - 10 AM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/aerobics-1.png'
                          alt='Aerobics-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Weight Lifting</h3>
                          <span>John Cina</span>
                          <div className='schedule_time'>
                            <h4>8 AM - 10 AM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/weight-1.png'
                          alt='weight-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Aerobics</h3>
                          <span>Tony Kiano</span>
                          <div className='schedule_time'>
                            <h4>8 AM - 10 AM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/aerobics-1.png'
                          alt='Aerobics-1'
                          className='img-fluid time-image'
                        />
                      </td>
                    </tr>
                    <tr>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Zumba Class</h3>
                          <span>Ketty Lumia</span>
                          <div className='schedule_time'>
                            <h4>10 AM - 12 PM</h4>
                          </div>
                        </div>
                        <img src='assets/images/schedule/zumba-1.png' alt='zumba-1' className='img-fluid time-image' />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Power Yoga</h3>
                          <span>Latina Hardi</span>
                          <div className='schedule_time'>
                            <h4>10 AM - 12 PM</h4>
                          </div>
                        </div>
                        <img src='assets/images/schedule/yoga-1.png' alt='yoga-1' className='img-fluid time-image' />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Power Yoga</h3>
                          <span>Latina Hardi</span>
                          <div className='schedule_time'>
                            <h4>10 AM - 12 PM</h4>
                          </div>
                        </div>
                        <img src='assets/images/schedule/yoga-1.png' alt='yoga-1' className='img-fluid time-image' />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Cardio</h3>
                          <span>Maria Anderson</span>
                          <div className='schedule_time'>
                            <h4>10 AM - 12 PM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/cardio-1.png'
                          alt='cardio-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Aerobics</h3>
                          <span>Tony Kiano</span>
                          <div className='schedule_time'>
                            <h4>10 AM - 12 PM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/aerobics-1.png'
                          alt='Aerobics-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Power Yoga</h3>
                          <span>Latina Hardi</span>
                          <div className='schedule_time'>
                            <h4>10 AM - 12 PM</h4>
                          </div>
                        </div>
                        <img src='assets/images/schedule/yoga-1.png' alt='yoga-1' className='img-fluid time-image' />
                      </td>
                    </tr>
                    <tr>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Cardio</h3>
                          <span>Maria Anderson</span>
                          <div className='schedule_time'>
                            <h4>12 PM - 2 PM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/cardio-1.png'
                          alt='cardio-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Boxing</h3>
                          <span>Steven Smith</span>
                          <div className='schedule_time'>
                            <h4>12 PM - 2 PM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/boxing-1.png'
                          alt='boxing-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Zumba Class</h3>
                          <span>Ketty Lumia</span>
                          <div className='schedule_time'>
                            <h4>12 PM - 2 PM</h4>
                          </div>
                        </div>
                        <img src='assets/images/schedule/zumba-1.png' alt='zumba-1' className='img-fluid time-image' />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Weight Lifting</h3>
                          <span>John Cina</span>
                          <div className='schedule_time'>
                            <h4>12 PM - 2 PM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/weight-1.png'
                          alt='weight-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Cardio</h3>
                          <span>Maria Anderson</span>
                          <div className='schedule_time'>
                            <h4>12 PM - 2 PM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/cardio-1.png'
                          alt='cardio-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Boxing</h3>
                          <span>Steven Smith</span>
                          <div className='schedule_time'>
                            <h4>12 PM - 2 PM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/boxing-1.png'
                          alt='boxing-1'
                          className='img-fluid time-image'
                        />
                      </td>
                    </tr>
                    <tr>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Boxing</h3>
                          <span>Steven Smith</span>
                          <div className='schedule_time'>
                            <h4>2 PM - 4 PM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/boxing-1.png'
                          alt='boxing-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Zumba Class</h3>
                          <span>Ketty Lumia</span>
                          <div className='schedule_time'>
                            <h4>2 PM - 4 PM</h4>
                          </div>
                        </div>
                        <img src='assets/images/schedule/zumba-1.png' alt='zumba-1' className='img-fluid time-image' />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Cardio</h3>
                          <span>Maria Anderson</span>
                          <div className='schedule_time'>
                            <h4>2 PM - 4 PM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/cardio-1.png'
                          alt='cardio-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Power Yoga</h3>
                          <span>Latina Hardi</span>
                          <div className='schedule_time'>
                            <h4>2 PM - 4 PM</h4>
                          </div>
                        </div>
                        <img src='assets/images/schedule/yoga-1.png' alt='yoga-1' className='img-fluid time-image' />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Boxing</h3>
                          <span>Steven Smith</span>
                          <div className='schedule_time'>
                            <h4>2 PM - 4 PM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/boxing-1.png'
                          alt='boxing-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Zumba Class</h3>
                          <span>Ketty Lumia</span>
                          <div className='schedule_time'>
                            <h4>2 PM - 4 PM</h4>
                          </div>
                        </div>
                        <img src='assets/images/schedule/zumba-1.png' alt='zumba-1' className='img-fluid time-image' />
                      </td>
                    </tr>
                    <tr>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Power Yoga</h3>
                          <span>Latina Hardi</span>
                          <div className='schedule_time'>
                            <h4>4 PM - 6 PM</h4>
                          </div>
                        </div>
                        <img src='assets/images/schedule/yoga-1.png' alt='yoga-1' className='img-fluid time-image' />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Weight Lifting</h3>
                          <span>John Cina</span>
                          <div className='schedule_time'>
                            <h4>4 PM - 6 PM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/weight-1.png'
                          alt='weight-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Aerobics</h3>
                          <span>Tony Kiano</span>
                          <div className='schedule_time'>
                            <h4>4 PM - 6 PM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/aerobics-1.png'
                          alt='Aerobics-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Boxing</h3>
                          <span>Steven Smith</span>
                          <div className='schedule_time'>
                            <h4>4 PM - 6 PM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/boxing-1.png'
                          alt='boxing-1'
                          className='img-fluid time-image'
                        />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Power Yoga</h3>
                          <span>Latina Hardi</span>
                          <div className='schedule_time'>
                            <h4>4 PM - 6 PM</h4>
                          </div>
                        </div>
                        <img src='assets/images/schedule/yoga-1.png' alt='yoga-1' className='img-fluid time-image' />
                      </td>
                      <td className='body_detail'>
                        <div className='tabel_content'>
                          <h3>Weight Lifting</h3>
                          <span>John Cina</span>
                          <div className='schedule_time'>
                            <h4>4 PM - 6 PM</h4>
                          </div>
                        </div>
                        <img
                          src='assets/images/schedule/weight-1.png'
                          alt='weight-1'
                          className='img-fluid time-image'
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Classes
