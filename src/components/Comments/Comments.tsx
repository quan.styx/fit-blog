import { Blog as blogType, Comment } from '../../types/blog.type'
interface CommentsType {
  comments: Comment[]
}
export default function Comments(props: CommentsType) {
  return (
    <div className='blog_comment-area' data-aos='fade-up' data-aos-easing='linear' data-aos-duration={1000}>
      <div className='container'>
        <h2>Comments [{props.comments.length}]</h2>
        <div className='comment-list'>
          {props.comments.map((item: Comment) => (
            <div>
              <div className='comment d-flex'>
                <div className='thumbnail'>
                  <img width={100} height={100} src='/blog/images/comment/comment-3.png' alt='Blog Comment3' />
                </div>
                <div className='comment-content'>
                  <div className='title'>{item.name}</div>
                  <div className='comment-date'>
                    <span>{item.date}</span>
                    <button className='reply-btn'>
                      <svg width={18} height={18} viewBox='0 0 18 18' fill='none' xmlns='http://www.w3.org/2000/svg'>
                        <path
                          d='M8.25 15L0.75 9L8.25 3V6.75C12.3923 6.75 15.75 10.1077 15.75 14.25C15.75 14.4548 15.7425 14.6572 15.726 14.8575C14.595 12.7125 12.3435 11.25 9.75 11.25H8.25V15Z'
                          fill='#EA1C29'
                        />
                      </svg>
                      Replies
                    </button>
                  </div>
                  <p>{item.content}</p>
                </div>
              </div>
              {item.replies.map((reply: any) => (
                <div className='comment comment-reply d-flex'>
                  <div className='thumbnail'>
                    <img width={100} height={100} src='/blog/images/comment/comment-2.png' alt='Blog Comment2' />
                  </div>
                  <div className='comment-content'>
                    <div className='title'>{reply.name}</div>
                    <div className='comment-date'>
                      <span>{reply.date}</span>
                    </div>
                    <p>{reply.content}</p>
                  </div>
                </div>
              ))}
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}
