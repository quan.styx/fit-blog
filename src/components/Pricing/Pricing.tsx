function Pricing() {
  return (
    <div className='fitzaro-table p-10' id='price'>
      <div className='container'>
        <div className='table_heading text-center'>
          <span className='sub_heading'>Pricing Table</span>
          <h2 className='mb-60'>Choose Your Pricing Plan</h2>
        </div>
        <div className='row' data-aos='fade-up' data-aos-easing='linear' data-aos-duration={1000}>
          <div className='col-12 col-lg-4 col-md-4'>
            <div className='pricing_data'>
              <div className='table_img'>
                <img src='assets/images/pricing/pricing-img1.png' alt='pricing1' className='img-fluid' />
              </div>
              <div className='price-data'>
                <span>STANDARD PLAN</span>
                <h3 className='m-0'>
                  $29<span className='month-text'>/Month</span>
                </h3>
              </div>
            </div>
            <div className='inn-price-plan'>
              <ol className='price_list'>
                <li>Unlimited Club access</li>
                <li>Group Attendance</li>
                <li>Gym Explore</li>
                <li>Visit Bath Complex</li>
                <li>Personal Trainer</li>
              </ol>
              <a className='button fitzaro_btn' href='javascript:void(0);'>
                Join Now
              </a>
            </div>
          </div>
          <div className='col-12 col-lg-4 col-md-4'>
            <div className='pricing_data'>
              <div className='table_img'>
                <img src='assets/images/pricing/pricing-img2.png' alt='pricing2' className='img-fluid' />
              </div>
              <div className='price-data'>
                <span>PREMIUM PLAN</span>
                <h3 className='m-0'>
                  $39<span className='month-text'>/Month</span>
                </h3>
              </div>
            </div>
            <div className='inn-price-plan'>
              <ol className='price_list'>
                <li>Unlimited Club access</li>
                <li>Group Attendance</li>
                <li>Gym Explore</li>
                <li>Visit Bath Complex</li>
                <li>Personal Trainer</li>
              </ol>
              <a className='button fitzaro_btn' href='javascript:void(0);'>
                Join Now
              </a>
            </div>
          </div>
          <div className='col-12 col-lg-4 col-md-4'>
            <div className='pricing_data'>
              <div className='table_img'>
                <img src='assets/images/pricing/pricing-img3.png' alt='pricing3' className='img-fluid' />
              </div>
              <div className='price-data'>
                <span>PLATINUM PLAN</span>
                <h3 className='m-0'>
                  $49<span className='month-text'>/Month</span>
                </h3>
              </div>
            </div>
            <div className='inn-price-plan'>
              <ol className='price_list'>
                <li>Unlimited Club access</li>
                <li>Group Attendance</li>
                <li>Gym Explore</li>
                <li>Visit Bath Complex</li>
                <li>Personal Trainer</li>
              </ol>
              <a className='button fitzaro_btn' href='javascript:void(0);'>
                Join Now
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Pricing
