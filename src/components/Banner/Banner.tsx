function Banner() {
  return (
    <div className='fitzaro_banner' id='banner'>
      <div className='container'>
        <div
          className='col-12 col-lg-6 col-md-10 dark-content p-10'
          data-aos='fade-right'
          data-aos-easing='linear'
          data-aos-duration={1000}
        >
          <h2 className='m-0 p-8'>Are You Looking For a Personal Trainer For Training?</h2>
          <a className='button fitzaro_btn' href='#appointment'>
            Book Appointment
          </a>
        </div>
      </div>
    </div>
  )
}

export default Banner
