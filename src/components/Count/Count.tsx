import { useEffect, useState } from 'react'
interface CountType {
  value: number
  end: number
  className?: string | ''
  active: boolean
}
export default function Count({ value, end, className, active }: CountType) {
  const [counter, setCounter] = useState(value)

  useEffect(() => {
    if (counter < end && active) {
      const interval = setInterval(() => {
        setCounter((counter) => counter + 1)
      }, 10)
      return () => {
        clearInterval(interval)
      }
    }
  }, [counter, active])

  return <div className={className}>{counter}</div>
}
