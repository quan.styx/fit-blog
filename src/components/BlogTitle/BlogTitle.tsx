import { Blog as blogType } from '../../types/blog.type'
interface blogTitleType {
  blog: blogType
}
export default function BlogTitle(props: blogTitleType) {
  return (
    <>
      {/* start first section */}
      <div className='first_section blog_page social-triangle' id='home'>
        <div className='parallax-overlay' />
        <div className='container'>
          <div className='banner_content col-12 col-lg-11 col-md-12' data-aos='zoom-in' data-aos-duration={1000}>
            <h1 className='main_tiltle m-0'>{props.blog.title}</h1>
            <p>{props.blog.subTitle}</p>
            <a className='button fitzaro_btn' href='#about'>
              Read More
            </a>
          </div>
          <a href='#about' className='scroll-down'>
            <span className='scroll-arrow arrow1' />
            <span className='scroll-arrow arrow2' />
            <span className='scroll-arrow arrow3' />
          </a>
        </div>
        <div className='slide_social_ic' data-aos='fade-up-left' data-aos-duration={1000}>
          <div className='triangle-1' />
          <div className='banner_social'>
            <a href='https://www.facebook.com/' target='_blank'>
              FB
            </a>
            <a href='https://www.instagram.com/' target='_blank'>
              IG
            </a>
            <a href='https://www.youtube.com/' target='_blank'>
              YT
            </a>
          </div>
        </div>
      </div>

      {/* end first section */}
    </>
  )
}
