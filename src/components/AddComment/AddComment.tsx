export default function AddComment() {
  return (
    <div className='comment-form p-10'>
      <div className='contact-form container' data-aos='fade-right' data-aos-easing='linear' data-aos-duration={1000}>
        <h2>Leave A Reply</h2>
        <form className='leave-comment row'>
          <div className='col-12 col-md-6'>
            <div className='comment-icon'>
              <i className='ri-user-line' />
              <input type='text' placeholder='Enter Your Name' className='comment-form-control' />
            </div>
          </div>
          <div className='col-12 col-md-6'>
            <div className='comment-icon'>
              <i className='ri-mail-line' />
              <input type='email' className='comment-form-control' placeholder='Enter Your Email' />
            </div>
          </div>
          <div className='col-12'>
            <div className='comment-icon'>
              <i className='ri-edit-line' />
              <textarea
                className='comment-form-control'
                placeholder='Write Your Comment'
                spellCheck='false'
                defaultValue={''}
              />
            </div>
          </div>
          <div className='col-12'>
            <label>
              <input type='checkbox' className='comment-form-control checkbox' name='checkbox' />
              Save My Name, Email, And Website In This Browser For The Next Time I Comment.
            </label>
          </div>
          <div className='col-12 comment-btn'>
            <button type='submit' className='button fitzaro_btn'>
              Post Comment
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}
