export interface User {
  userId?: string
  userName: string
  phone?: number
  photo?: string
  email: string
  memberType?: string
  token: string
}
