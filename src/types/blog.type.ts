export interface Post {
  title: string
  description: string
  publishDate: string
  id: string
  featuredImage: string
  published: boolean
}

export interface Blog {
  title: string
  mainImage: string
  slugTitle: string
  subTitle: string
  datePosted: string
  missionText: string
  subPostTitle: string
  subPostData: string
  subPostList1: string[]
  subPostList2: string[]
  images: string[]
  subDetail: string
  quote: string
  author: string
  comments: Comment[]
}

export interface Comment{
  name: string,
  date: string,
  content: string,
  replies: Object[]
}
