import Header from '../../components/Header'
import Preloader from '../../components/Preloader'
import TopBottomScroller from '../../components/TopBottomScroller'
import Blogs from '../../components/Blogs'
import Subscribe from '../../components/Subscribe'
import Footer from '../../components/Footer'
import BlogTitle from '../../components/BlogTitle'
import BlogPostDetail from 'components/BlogPostDetail'
import Comments from '../../components/Comments'
import AddComment from '../../components/AddComment'
import { listPosts } from '../../constants/blogPosts'
import './blog.css'
import { useEffect, useRef, useState } from 'react'
import '../../css/landing.css'
import '../../css/all.min.css'
import '../../css/bootstrap.min.css'
import '../../css/animate.css'
import '../../css/aos.css'
import '../../css/custom.css'
import '../../css/media-query.css'
import 'aos/dist/aos.css'
import { useParams } from 'react-router-dom'
import { Blog as blogType } from '../../types/blog.type'
var AOS = require('aos')

const initialBlog: blogType = {
  title: '',
  mainImage: '',
  slugTitle: '',
  subTitle: '',
  datePosted: '',
  missionText: '',
  subPostTitle: '',
  subPostData: '',
  subPostList1: [],
  subPostList2: [],
  images: [],
  subDetail: '',
  quote: '',
  author: '',
  comments: []
}

export default function Blog() {
  const { id } = useParams()
  let [blog, setBlog] = useState<blogType>(initialBlog)
  useEffect(() => {
    let post = listPosts.find((item) => item.slugTitle === id)
    setBlog(post ? post : initialBlog)
  }, [id])
  const navLocation = useRef('#home')
  AOS.init({
    once: true,
    disable: function () {
      var maxWidth = 767
      return window.innerWidth < maxWidth
    }
  })

  return blog === initialBlog ? (
    <div>Blog not found !!!</div>
  ) : (
    <div id='wrap'>
      <div className='mouseCursor cursor-outer'></div>
      <div className='mouseCursor cursor-inner'>view</div>
      <Header for='blog' position={navLocation.current} />
      <BlogTitle blog={blog} />
      <BlogPostDetail blog={blog} />
      <Blogs />
      <Comments comments={blog.comments} />
      <AddComment />
      <Subscribe />
      <Footer />
      <TopBottomScroller />
    </div>
  )
}
