import { createSlice, PayloadAction } from '@reduxjs/toolkit'

interface LandingState {
  isViewing: string
}

const initialState: LandingState = {
  // userId: -1,
  // userName: '',
  // memberType: '',
  // fistName: '',
  // lastName: '',
  isViewing: 'home'
}

const landingSlice = createSlice({
  name: 'landing',
  initialState,
  reducers: {
    login: (state, action: PayloadAction<LandingState>) => {
      state = action.payload
    },
    logout: (state, action: PayloadAction<LandingState>) => {
      state = initialState
    }
  }
})

const landingReducer = landingSlice.reducer
export const { login, logout } = landingSlice.actions
export default landingReducer
