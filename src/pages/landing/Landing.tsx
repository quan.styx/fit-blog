import firebase from '../../services/firebase'
import Header from '../../components/Header'
import Preloader from '../../components/Preloader'
import Slider from '../../components/Slider'
import About from '../../components/About'
import Video from '../../components/Video'
import Service from '../../components/Service'
import Counter from '../../components/Counter'
import Schedule from '../../components/Schedule'
import Testimonial from '../../components/Testimonial'
import Pricing from '../../components/Pricing'
import Trainer from '../../components/Trainer'
import Banner from '../../components/Banner'
import Appointment from '../../components/Appointment'
import Blogs from '../../components/Blogs'
import Footer from '../../components/Footer'
import Subscribe from '../../components/Subscribe'
import PlayVideo from '../../components/PlayVideo'
import TopBottomScroller from '../../components/TopBottomScroller'
import FloatingLoginButton from '../../components/FloatingLoginButton'
import FloatingLogoutButton from '../../components/FloatingLogoutButton'
import { RootState, useAppDispatch } from '../../store'
import { useSelector } from 'react-redux'
import { useEffect, useRef, useState } from 'react'
import '../../css/landing.css'
import '../../css/all.min.css'
import '../../css/bootstrap.min.css'
import '../../css/animate.css'
import '../../css/aos.css'
import '../../css/slider.css'
import '../../css/custom.css'
import '../../css/media-query.css'
import 'aos/dist/aos.css'
import { getAuth } from 'firebase/auth'
import { userInfo } from 'os'
var AOS = require('aos')

export default function Landing() {
  let [isLoading, setLoading] = useState(false)
  let [user, setUser] = useState<Object>({})
  let userInfo = useSelector((state: RootState) => state.auth.currentUser)
  useEffect(() => {
    getAuth(firebase).onAuthStateChanged((user: any) => {
      setUser(user)
    })
  }, [])
  //console.log(user)
  const navLocation = useRef('#home')
  AOS.init({
    once: true,
    disable: function () {
      var maxWidth = 767
      return window.innerWidth < maxWidth
    }
  })
  useEffect(() => {
    const currentScrollPos = document.documentElement.scrollTop
  })
  // Cần check lại chỗ này
  // useEffect(() => {
  //   let lFollowX = 0,
  //     lFollowY = 0,
  //     x = 0,
  //     y = 0,
  //     friction = 1 / 30

  //   function moveBackground() {
  //     x += (lFollowX - x) * friction
  //     y += (lFollowY - y) * friction
  //     console.log(x)
  //     //  translate = 'translateX(' + x + 'px, ' + y + 'px)';
  //     let translate = 'translateX(' + x + 'px) translateY(' + y + 'px)'

  //     document.querySelectorAll('.animate-this').forEach((e) => {
  //       e.setAttribute('style', `transform: ${translate}`)
  //     })
  //   }
  //   window.requestAnimationFrame(moveBackground)

  //   window.addEventListener('mousemove', BackgroundHandle)
  //   function BackgroundHandle(e: any) {
  //     var isHovered = document.querySelectorAll('.animate-this:hover').length > 0

  //     if (!isHovered) {
  //       var lMouseX = Math.max(-100, Math.min(100, window.innerWidth / 2 - e.clientX)),
  //         lMouseY = Math.max(-100, Math.min(100, window.innerHeight / 2 - e.clientY))

  //       lFollowX = (20 * lMouseX) / 100
  //       lFollowY = (10 * lMouseY) / 100
  //     }
  //   }
  //   moveBackground()
  // })

  return isLoading ? (
    <Preloader />
  ) : (
    <div id='wrap'>
      <div className='mouseCursor cursor-outer'></div>
      <div className='mouseCursor cursor-inner'>view</div>
      <Header for='home' position={navLocation.current} />
      <Slider />
      <About />
      <Video />
      <Service />
      <Counter />
      <Schedule />
      <Testimonial />
      <Pricing />
      <Banner />
      <Trainer />
      <Appointment />
      <Blogs />
      <Subscribe />
      <Footer />
      <PlayVideo />
      <TopBottomScroller />
      {user ? <FloatingLogoutButton name={userInfo ? userInfo?.userName : ''} /> : <FloatingLoginButton />}
    </div>
  )
}
