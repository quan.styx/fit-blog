import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { Post } from '../../types/blog.type'
//Query : Dùng cho get
//Mutation: Dùng cho POST, PUT, PATCH, DELETE
export const landingApi = createApi({
  reducerPath: 'landingApi', //Tên field trong redux state
  tagTypes: ['Posts'],
  baseQuery: fetchBaseQuery({
    baseUrl: 'http://localhost:3001/',
    prepareHeaders(headers) {
      headers.set('Authorization', 'Bearer ABCXYZ')
      return headers
    }
  }),
  endpoints: (build) => ({
    getPosts: build.query<Post[], void>({
      query: () => 'posts',
      providesTags(result) {
        if (result) {
          return [...result.map(({ id }) => ({ type: 'Posts' as const, id })), { type: 'Posts' as const, id: 'LIST' }]
        }
        return [{ type: 'Posts' as const, id: 'LIST' }]
      }
    }), // Method không có argument
    getPost: build.query<Post, string>({
      query: (id) => ({
        url: `posts/${id}`,
        headers: {
          pagination: 'false'
        },
        params: {
          first_name: 'Quan',
          'last-name': 'Nguyen'
        }
      })
    }), // Method có argument
    addPost: build.mutation<Post, Omit<Post, 'id'>>({
      query(body) {
        return {
          url: 'posts',
          method: 'POST',
          body
        }
      },
      invalidatesTags: (result, error, body) => (!error ? [{ type: 'Posts', id: 'LIST' }] : [])
    }),
    updatePost: build.mutation<Post, { id: string; body: Post }>({
      query(data) {
        return {
          url: `posts/${data.id}`,
          method: 'PATCH',
          body: data.body
        }
      },
      invalidatesTags: (result, error, data) => (!error ? [{ type: 'Posts', id: data.id }] : [])
    }),
    deletePost: build.mutation<{}, string>({
      query(id) {
        return {
          url: `posts/${id}`,
          method: 'DELETE'
        }
      },

      invalidatesTags: (result, error, id) => (!error ? [{ type: 'Posts', id }] : [])
    })
  })
})

export const { useGetPostsQuery, useGetPostQuery, useAddPostMutation, useUpdatePostMutation, useDeletePostMutation } =
  landingApi
