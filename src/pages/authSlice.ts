import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { User } from '../types/user.type'


interface AuthState {
  isLoggedIn: boolean
  logging?: boolean
  currentUser?: User
  token: string
}

const initialState: AuthState = {
  isLoggedIn: false,
  logging: false,
  currentUser: undefined,
  token: ''
}

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login: (state, action: PayloadAction<AuthState>) => {
      state.logging = true
    },
    loginSuccess: (state, action: PayloadAction<User>) => {
      state.logging = true
      state.logging = false
    },
    logout: (state, action: PayloadAction<User>) => {
      state = initialState
    },
    loginWithGoogleFirebase: (state, action: PayloadAction<User>) => {
      state.isLoggedIn = true
      state.currentUser = action.payload
    }
  }
})

const authReducer = authSlice.reducer
export const { login, loginSuccess, loginWithGoogleFirebase, logout } = authSlice.actions
export default authReducer
