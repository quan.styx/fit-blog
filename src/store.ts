import { configureStore } from '@reduxjs/toolkit'
import landingReducer from './pages/landing/landing.slice'
import authReducer from './pages/authSlice'
import { useDispatch } from 'react-redux'
import { landingApi } from './pages/landing/landing.service'
import { setupListeners } from '@reduxjs/toolkit/query'
import { rtkQueryErrorLogger } from './middleware'

export const store = configureStore({
  reducer: { landing: landingReducer, auth: authReducer, [landingApi.reducerPath]: landingApi.reducer },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(landingApi.middleware, rtkQueryErrorLogger)
})

// refetchOnFocus/ refetchOnReconnect
setupListeners(store.dispatch)

// Lấy RootState và AppDispatch từ store.
export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch

export const useAppDispatch = () => useDispatch<AppDispatch>()
