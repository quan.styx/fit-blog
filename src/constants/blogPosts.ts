/* eslint-disable */
export const listPosts = [
    {
        title: 'The 10 best exercises to do in your home',
        mainImage: '/assets/images/blog/blog-img1.png',
        slugTitle: 'the-10-best-exercises-to-do-in-your-home',
        subTitle: 'Elementum nulla lorem mauris arcu, posuere est amet. Cursus donec aenean scelerisque consectetur diam fermentum. Sagittis morbi a euismod eu. Odio ut amet lectus egestas habitasse sit in. Quis nunc volutpat purus mi tortor laoreet. Arcu, neque dictumst bibendum et convallis arcu auctor eros. Purus aenean facilisis commodo sit. Magna suspendisse nulla massa.',
        datePosted: 'June 05, 2022',
        missionText: 'Nulla porttitor volutpat nisi nunc enim. Vitae vulputate aliquam, in aliquet tortor. Lorem nec congue quis pharetra porttitor. Luctus tortor quam cursus adipiscing mattis volutpat. Eget et sit in ridiculus.<br><br>Purus accumsan et pulvinar scelerisque in. Feugiat hendrerit sed quis pharetra est vel, ut ut nulla. Tincidunt etiam pellentesque arcu, vitae justo iaculis purus volutpat. Amet, nisi tortor ligula pellentesque tellus at. Tortor et turpis aenean tristique ut placerat est.<br><br>Id leo cras tempus enim. Luctus accumsan massa feugiat sodales imperdiet iaculis facilisis vestibulum. Scelerisque eu faucibus faucibus elit elementum enim, libero. Cursus interdum vulputate volutpat consectetur. Nisl, pulvinar lectus odio scelerisque tincidunt integer feugiat at.',
        subPostTitle: 'Everybody wanna be a bodybuilder, but nobody wanna lift no heavy weight.',
        subPostData: 'Purus accumsan et pulvinar scelerisque in. Feugiat hendrerit sed quis pharetra est vel, ut ut nulla. Tincidunt etiam pellentesque arcu, vitae justo iaculis purus volutpat. Amet, nisi tortor ligula pellentesque tellus at. Tortor et turpis aenean tristique ut placerat est. Id leo cras tempus enim. Luctus accumsan massa feugiat sodales imperdiet iaculis facilisis vestibulum. Scelerisque eu faucibus faucibus elit elementum enim, libero. Cursus interdum vulputate volutpat consectetur. Nisl, pulvinar lectus odio scelerisque tincidunt integer feugiat at.',
        subPostList1: [
            'Duis quis odio quis dui sagittis laoreet.', 'Suspendisse tempus felis a libero mollis ultrices.','Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Suspendisse tempus felis a libero mollis ultrices.'
        ],
        subPostList2: [
            'Duis quis odio quis dui sagittis laoreet.', 'Suspendisse tempus felis a libero mollis ultrices.','Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Suspendisse tempus felis a libero mollis ultrices.'
        ],
        images: [
            'blog/images/blog_post1.png', 'blog/images/blog_post2.png', 'blog/images/blog_post3.png'
        ],
        subDetail: 'Nulla porttitor volutpat nisi nunc enim. Vitae vulputate aliquam, in aliquet tortor. Lorem nec congue quis pharetra porttitor. Luctus tortor quam cursus adipiscing mattis volutpat. Eget et sit in ridiculus.<br><br>Purus accumsan et pulvinar scelerisque in. Feugiat hendrerit sed quis pharetra est vel, ut ut nulla. Tincidunt etiam pellentesque arcu, vitae justo iaculis purus volutpat. Amet, nisi tortor ligula pellentesque tellus at. Tortor et turpis aenean tristique ut placerat est.<br><br>Id leo cras tempus enim. Luctus accumsan massa feugiat sodales imperdiet iaculis facilisis vestibulum. Scelerisque eu faucibus faucibus elit elementum enim, libero. Cursus interdum vulputate volutpat consectetur. Nisl, pulvinar lectus odio scelerisque tincidunt integer feugiat at.',
        quote: '“Bodybuilding is much like any other sport. To be successful, you must dedicate yourself 100% to your training, diet and mental approach.¨”',
        author: 'Arnold Schwarzenegger',
        comments: [
            {
                name: 'Andy Doe',
                date: 'December 19, 2021  AT 06:30 PM',
                content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                replies: [
                    {
                        name: 'Mary Louis',
                        date: 'December 19, 2021  AT 06:30 PM',
                        content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                    }
                ]
            },
            {
                name: 'Andy Doe',
                date: 'December 19, 2021  AT 06:30 PM',
                content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                replies: [
                    {
                        name: 'Mary Louis',
                        date: 'December 19, 2021  AT 06:30 PM',
                        content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                    }
                ]
            },
            {
                name: 'Andy Doe',
                date: 'December 19, 2021  AT 06:30 PM',
                content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                replies: []
            }
        ]
    },

    {
        title: 'Going to the gym for the first time doesn’t need to be daunting',
        mainImage: '/assets/images/blog/blog-img1.png',
        slugTitle: 'going-to-the-gym-for-the-first-time-doesnt-need-to-be-daunting',
        subTitle: 'Elementum nulla lorem mauris arcu, posuere est amet. Cursus donec aenean scelerisque consectetur diam fermentum. Sagittis morbi a euismod eu. Odio ut amet lectus egestas habitasse sit in. Quis nunc volutpat purus mi tortor laoreet. Arcu, neque dictumst bibendum et convallis arcu auctor eros. Purus aenean facilisis commodo sit. Magna suspendisse nulla massa.',
        datePosted: 'June 05, 2022',
        missionText: 'Nulla porttitor volutpat nisi nunc enim. Vitae vulputate aliquam, in aliquet tortor. Lorem nec congue quis pharetra porttitor. Luctus tortor quam cursus adipiscing mattis volutpat. Eget et sit in ridiculus.<br><br>Purus accumsan et pulvinar scelerisque in. Feugiat hendrerit sed quis pharetra est vel, ut ut nulla. Tincidunt etiam pellentesque arcu, vitae justo iaculis purus volutpat. Amet, nisi tortor ligula pellentesque tellus at. Tortor et turpis aenean tristique ut placerat est.<br><br>Id leo cras tempus enim. Luctus accumsan massa feugiat sodales imperdiet iaculis facilisis vestibulum. Scelerisque eu faucibus faucibus elit elementum enim, libero. Cursus interdum vulputate volutpat consectetur. Nisl, pulvinar lectus odio scelerisque tincidunt integer feugiat at.',
        subPostTitle: 'Everybody wanna be a bodybuilder, but nobody wanna lift no heavy weight.',
        subPostData: 'Purus accumsan et pulvinar scelerisque in. Feugiat hendrerit sed quis pharetra est vel, ut ut nulla. Tincidunt etiam pellentesque arcu, vitae justo iaculis purus volutpat. Amet, nisi tortor ligula pellentesque tellus at. Tortor et turpis aenean tristique ut placerat est. Id leo cras tempus enim. Luctus accumsan massa feugiat sodales imperdiet iaculis facilisis vestibulum. Scelerisque eu faucibus faucibus elit elementum enim, libero. Cursus interdum vulputate volutpat consectetur. Nisl, pulvinar lectus odio scelerisque tincidunt integer feugiat at.',
        subPostList1: [
            'Duis quis odio quis dui sagittis laoreet.', 'Suspendisse tempus felis a libero mollis ultrices.','Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Suspendisse tempus felis a libero mollis ultrices.'
        ],
        subPostList2: [
            'Duis quis odio quis dui sagittis laoreet.', 'Suspendisse tempus felis a libero mollis ultrices.','Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Suspendisse tempus felis a libero mollis ultrices.'
        ],
        images: [
            'blog/images/blog_post1.png', 'blog/images/blog_post2.png', 'blog/images/blog_post3.png'
        ],
        subDetail: 'Nulla porttitor volutpat nisi nunc enim. Vitae vulputate aliquam, in aliquet tortor. Lorem nec congue quis pharetra porttitor. Luctus tortor quam cursus adipiscing mattis volutpat. Eget et sit in ridiculus.<br><br>Purus accumsan et pulvinar scelerisque in. Feugiat hendrerit sed quis pharetra est vel, ut ut nulla. Tincidunt etiam pellentesque arcu, vitae justo iaculis purus volutpat. Amet, nisi tortor ligula pellentesque tellus at. Tortor et turpis aenean tristique ut placerat est.<br><br>Id leo cras tempus enim. Luctus accumsan massa feugiat sodales imperdiet iaculis facilisis vestibulum. Scelerisque eu faucibus faucibus elit elementum enim, libero. Cursus interdum vulputate volutpat consectetur. Nisl, pulvinar lectus odio scelerisque tincidunt integer feugiat at.',
        quote: '“Bodybuilding is much like any other sport. To be successful, you must dedicate yourself 100% to your training, diet and mental approach.¨”',
        author: 'Arnold Schwarzenegger',
        comments: [
            {
                name: 'Andy Doe',
                date: 'December 19, 2021  AT 06:30 PM',
                content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                replies: [
                    {
                        name: 'Mary Louis',
                        date: 'December 19, 2021  AT 06:30 PM',
                        content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                    }
                ]
            },
            {
                name: 'Andy Doe',
                date: 'December 19, 2021  AT 06:30 PM',
                content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                replies: [
                    {
                        name: 'Mary Louis',
                        date: 'December 19, 2021  AT 06:30 PM',
                        content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                    }
                ]
            },
            {
                name: 'Andy Doe',
                date: 'December 19, 2021  AT 06:30 PM',
                content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                replies: []
            }
        ]
    },

    {
        title: 'The interesting mental health benefits of exercise',
        mainImage: '/assets/images/blog/blog-img1.png',
        slugTitle: 'the-interesting-mental-health-benefits-of-exercise',
        subTitle: 'Elementum nulla lorem mauris arcu, posuere est amet. Cursus donec aenean scelerisque consectetur diam fermentum. Sagittis morbi a euismod eu. Odio ut amet lectus egestas habitasse sit in. Quis nunc volutpat purus mi tortor laoreet. Arcu, neque dictumst bibendum et convallis arcu auctor eros. Purus aenean facilisis commodo sit. Magna suspendisse nulla massa.',
        datePosted: 'June 05, 2022',
        missionText: 'Nulla porttitor volutpat nisi nunc enim. Vitae vulputate aliquam, in aliquet tortor. Lorem nec congue quis pharetra porttitor. Luctus tortor quam cursus adipiscing mattis volutpat. Eget et sit in ridiculus.<br><br>Purus accumsan et pulvinar scelerisque in. Feugiat hendrerit sed quis pharetra est vel, ut ut nulla. Tincidunt etiam pellentesque arcu, vitae justo iaculis purus volutpat. Amet, nisi tortor ligula pellentesque tellus at. Tortor et turpis aenean tristique ut placerat est.<br><br>Id leo cras tempus enim. Luctus accumsan massa feugiat sodales imperdiet iaculis facilisis vestibulum. Scelerisque eu faucibus faucibus elit elementum enim, libero. Cursus interdum vulputate volutpat consectetur. Nisl, pulvinar lectus odio scelerisque tincidunt integer feugiat at.',
        subPostTitle: 'Everybody wanna be a bodybuilder, but nobody wanna lift no heavy weight.',
        subPostData: 'Purus accumsan et pulvinar scelerisque in. Feugiat hendrerit sed quis pharetra est vel, ut ut nulla. Tincidunt etiam pellentesque arcu, vitae justo iaculis purus volutpat. Amet, nisi tortor ligula pellentesque tellus at. Tortor et turpis aenean tristique ut placerat est. Id leo cras tempus enim. Luctus accumsan massa feugiat sodales imperdiet iaculis facilisis vestibulum. Scelerisque eu faucibus faucibus elit elementum enim, libero. Cursus interdum vulputate volutpat consectetur. Nisl, pulvinar lectus odio scelerisque tincidunt integer feugiat at.',
        subPostList1: [
            'Duis quis odio quis dui sagittis laoreet.', 'Suspendisse tempus felis a libero mollis ultrices.','Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Suspendisse tempus felis a libero mollis ultrices.'
        ],
        subPostList2: [
            'Duis quis odio quis dui sagittis laoreet.', 'Suspendisse tempus felis a libero mollis ultrices.','Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Suspendisse tempus felis a libero mollis ultrices.'
        ],
        images: [
            'blog/images/blog_post1.png', 'blog/images/blog_post2.png', 'blog/images/blog_post3.png'
        ],
        subDetail: 'Nulla porttitor volutpat nisi nunc enim. Vitae vulputate aliquam, in aliquet tortor. Lorem nec congue quis pharetra porttitor. Luctus tortor quam cursus adipiscing mattis volutpat. Eget et sit in ridiculus.<br><br>Purus accumsan et pulvinar scelerisque in. Feugiat hendrerit sed quis pharetra est vel, ut ut nulla. Tincidunt etiam pellentesque arcu, vitae justo iaculis purus volutpat. Amet, nisi tortor ligula pellentesque tellus at. Tortor et turpis aenean tristique ut placerat est.<br><br>Id leo cras tempus enim. Luctus accumsan massa feugiat sodales imperdiet iaculis facilisis vestibulum. Scelerisque eu faucibus faucibus elit elementum enim, libero. Cursus interdum vulputate volutpat consectetur. Nisl, pulvinar lectus odio scelerisque tincidunt integer feugiat at.',
        quote: '“Bodybuilding is much like any other sport. To be successful, you must dedicate yourself 100% to your training, diet and mental approach.¨”',
        author: 'Arnold Schwarzenegger',
        comments: [
            {
                name: 'Andy Doe',
                date: 'December 19, 2021  AT 06:30 PM',
                content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                replies: [
                    {
                        name: 'Mary Louis',
                        date: 'December 19, 2021  AT 06:30 PM',
                        content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                    }
                ]
            },
            {
                name: 'Andy Doe',
                date: 'December 19, 2021  AT 06:30 PM',
                content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                replies: [
                    {
                        name: 'Mary Louis',
                        date: 'December 19, 2021  AT 06:30 PM',
                        content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                    }
                ]
            },
            {
                name: 'Andy Doe',
                date: 'December 19, 2021  AT 06:30 PM',
                content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                replies: []
            }
        ]
    },

    {
        title: 'Going to the gym for the first time',
        mainImage: '/assets/images/blog/blog-img1.png',
        slugTitle: 'going-to-the-gym-for-the-first-time',
        subTitle: 'Elementum nulla lorem mauris arcu, posuere est amet. Cursus donec aenean scelerisque consectetur diam fermentum. Sagittis morbi a euismod eu. Odio ut amet lectus egestas habitasse sit in. Quis nunc volutpat purus mi tortor laoreet. Arcu, neque dictumst bibendum et convallis arcu auctor eros. Purus aenean facilisis commodo sit. Magna suspendisse nulla massa.',
        datePosted: 'June 05, 2022',
        missionText: 'Nulla porttitor volutpat nisi nunc enim. Vitae vulputate aliquam, in aliquet tortor. Lorem nec congue quis pharetra porttitor. Luctus tortor quam cursus adipiscing mattis volutpat. Eget et sit in ridiculus.<br><br>Purus accumsan et pulvinar scelerisque in. Feugiat hendrerit sed quis pharetra est vel, ut ut nulla. Tincidunt etiam pellentesque arcu, vitae justo iaculis purus volutpat. Amet, nisi tortor ligula pellentesque tellus at. Tortor et turpis aenean tristique ut placerat est.<br><br>Id leo cras tempus enim. Luctus accumsan massa feugiat sodales imperdiet iaculis facilisis vestibulum. Scelerisque eu faucibus faucibus elit elementum enim, libero. Cursus interdum vulputate volutpat consectetur. Nisl, pulvinar lectus odio scelerisque tincidunt integer feugiat at.',
        subPostTitle: 'Everybody wanna be a bodybuilder, but nobody wanna lift no heavy weight.',
        subPostData: 'Purus accumsan et pulvinar scelerisque in. Feugiat hendrerit sed quis pharetra est vel, ut ut nulla. Tincidunt etiam pellentesque arcu, vitae justo iaculis purus volutpat. Amet, nisi tortor ligula pellentesque tellus at. Tortor et turpis aenean tristique ut placerat est. Id leo cras tempus enim. Luctus accumsan massa feugiat sodales imperdiet iaculis facilisis vestibulum. Scelerisque eu faucibus faucibus elit elementum enim, libero. Cursus interdum vulputate volutpat consectetur. Nisl, pulvinar lectus odio scelerisque tincidunt integer feugiat at.',
        subPostList1: [
            'Duis quis odio quis dui sagittis laoreet.', 'Suspendisse tempus felis a libero mollis ultrices.','Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Suspendisse tempus felis a libero mollis ultrices.'
        ],
        subPostList2: [
            'Duis quis odio quis dui sagittis laoreet.', 'Suspendisse tempus felis a libero mollis ultrices.','Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Suspendisse tempus felis a libero mollis ultrices.'
        ],
        images: [
            'blog/images/blog_post1.png', 'blog/images/blog_post2.png', 'blog/images/blog_post3.png'
        ],
        subDetail: 'Nulla porttitor volutpat nisi nunc enim. Vitae vulputate aliquam, in aliquet tortor. Lorem nec congue quis pharetra porttitor. Luctus tortor quam cursus adipiscing mattis volutpat. Eget et sit in ridiculus.<br><br>Purus accumsan et pulvinar scelerisque in. Feugiat hendrerit sed quis pharetra est vel, ut ut nulla. Tincidunt etiam pellentesque arcu, vitae justo iaculis purus volutpat. Amet, nisi tortor ligula pellentesque tellus at. Tortor et turpis aenean tristique ut placerat est.<br><br>Id leo cras tempus enim. Luctus accumsan massa feugiat sodales imperdiet iaculis facilisis vestibulum. Scelerisque eu faucibus faucibus elit elementum enim, libero. Cursus interdum vulputate volutpat consectetur. Nisl, pulvinar lectus odio scelerisque tincidunt integer feugiat at.',
        quote: '“Bodybuilding is much like any other sport. To be successful, you must dedicate yourself 100% to your training, diet and mental approach.¨”',
        author: 'Arnold Schwarzenegger',
        comments: [
            {
                name: 'Andy Doe',
                date: 'December 19, 2021  AT 06:30 PM',
                content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                replies: [
                    {
                        name: 'Mary Louis',
                        date: 'December 19, 2021  AT 06:30 PM',
                        content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                    }
                ]
            },
            {
                name: 'Andy Doe',
                date: 'December 19, 2021  AT 06:30 PM',
                content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                replies: [
                    {
                        name: 'Mary Louis',
                        date: 'December 19, 2021  AT 06:30 PM',
                        content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                    }
                ]
            },
            {
                name: 'Andy Doe',
                date: 'December 19, 2021  AT 06:30 PM',
                content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                replies: []
            }
        ]
    },

    {
        title: '8 Tips How to Prepare Meals Fast and Easy',
        mainImage: '/assets/images/blog/blog-img1.png',
        slugTitle: '8-tips-how-to-prepare-meals-fast-and-easy',
        subTitle: 'Elementum nulla lorem mauris arcu, posuere est amet. Cursus donec aenean scelerisque consectetur diam fermentum. Sagittis morbi a euismod eu. Odio ut amet lectus egestas habitasse sit in. Quis nunc volutpat purus mi tortor laoreet. Arcu, neque dictumst bibendum et convallis arcu auctor eros. Purus aenean facilisis commodo sit. Magna suspendisse nulla massa.',
        datePosted: 'June 05, 2022',
        missionText: 'Nulla porttitor volutpat nisi nunc enim. Vitae vulputate aliquam, in aliquet tortor. Lorem nec congue quis pharetra porttitor. Luctus tortor quam cursus adipiscing mattis volutpat. Eget et sit in ridiculus.<br><br>Purus accumsan et pulvinar scelerisque in. Feugiat hendrerit sed quis pharetra est vel, ut ut nulla. Tincidunt etiam pellentesque arcu, vitae justo iaculis purus volutpat. Amet, nisi tortor ligula pellentesque tellus at. Tortor et turpis aenean tristique ut placerat est.<br><br>Id leo cras tempus enim. Luctus accumsan massa feugiat sodales imperdiet iaculis facilisis vestibulum. Scelerisque eu faucibus faucibus elit elementum enim, libero. Cursus interdum vulputate volutpat consectetur. Nisl, pulvinar lectus odio scelerisque tincidunt integer feugiat at.',
        subPostTitle: 'Everybody wanna be a bodybuilder, but nobody wanna lift no heavy weight.',
        subPostData: 'Purus accumsan et pulvinar scelerisque in. Feugiat hendrerit sed quis pharetra est vel, ut ut nulla. Tincidunt etiam pellentesque arcu, vitae justo iaculis purus volutpat. Amet, nisi tortor ligula pellentesque tellus at. Tortor et turpis aenean tristique ut placerat est. Id leo cras tempus enim. Luctus accumsan massa feugiat sodales imperdiet iaculis facilisis vestibulum. Scelerisque eu faucibus faucibus elit elementum enim, libero. Cursus interdum vulputate volutpat consectetur. Nisl, pulvinar lectus odio scelerisque tincidunt integer feugiat at.',
        subPostList1: [
            'Duis quis odio quis dui sagittis laoreet.', 'Suspendisse tempus felis a libero mollis ultrices.','Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Suspendisse tempus felis a libero mollis ultrices.'
        ],
        subPostList2: [
            'Duis quis odio quis dui sagittis laoreet.', 'Suspendisse tempus felis a libero mollis ultrices.','Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Suspendisse tempus felis a libero mollis ultrices.'
        ],
        images: [
            'blog/images/blog_post1.png', 'blog/images/blog_post2.png', 'blog/images/blog_post3.png'
        ],
        subDetail: 'Nulla porttitor volutpat nisi nunc enim. Vitae vulputate aliquam, in aliquet tortor. Lorem nec congue quis pharetra porttitor. Luctus tortor quam cursus adipiscing mattis volutpat. Eget et sit in ridiculus.<br><br>Purus accumsan et pulvinar scelerisque in. Feugiat hendrerit sed quis pharetra est vel, ut ut nulla. Tincidunt etiam pellentesque arcu, vitae justo iaculis purus volutpat. Amet, nisi tortor ligula pellentesque tellus at. Tortor et turpis aenean tristique ut placerat est.<br><br>Id leo cras tempus enim. Luctus accumsan massa feugiat sodales imperdiet iaculis facilisis vestibulum. Scelerisque eu faucibus faucibus elit elementum enim, libero. Cursus interdum vulputate volutpat consectetur. Nisl, pulvinar lectus odio scelerisque tincidunt integer feugiat at.',
        quote: '“Bodybuilding is much like any other sport. To be successful, you must dedicate yourself 100% to your training, diet and mental approach.¨”',
        author: 'Arnold Schwarzenegger',
        comments: [
            {
                name: 'Andy Doe',
                date: 'December 19, 2021  AT 06:30 PM',
                content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                replies: [
                    {
                        name: 'Mary Louis',
                        date: 'December 19, 2021  AT 06:30 PM',
                        content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                    }
                ]
            },
            {
                name: 'Andy Doe',
                date: 'December 19, 2021  AT 06:30 PM',
                content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                replies: [
                    {
                        name: 'Mary Louis',
                        date: 'December 19, 2021  AT 06:30 PM',
                        content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                    }
                ]
            },
            {
                name: 'Andy Doe',
                date: 'December 19, 2021  AT 06:30 PM',
                content: 'Threm ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo et dolore magna aliqua. Ut enim ad minim veniamquis nostrud.',
                replies: []
            }
        ]
    }
]
