// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app'
import { getAnalytics } from 'firebase/analytics'
import { getAuth } from 'firebase/auth'
import { GoogleAuthProvider, signInWithPopup } from 'firebase/auth'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyB_AtpPgnkzSZKbzqDhL2PwhH00gngCKrg',
  authDomain: 'fit-blog-d8150.firebaseapp.com',
  projectId: 'fit-blog-d8150',
  storageBucket: 'fit-blog-d8150.appspot.com',
  messagingSenderId: '645656053111',
  appId: '1:645656053111:web:9a5f3c513e5136f0290d72',
  measurementId: 'G-MTV3LFJN63'
}

// Initialize Firebase
const firebase = initializeApp(firebaseConfig)

const analytics = getAnalytics(firebase)
export const auth = getAuth(firebase)
const provider = new GoogleAuthProvider()
provider.setCustomParameters({ prompt: 'select_account' })

export const signInWithGoogle = () => signInWithPopup(auth, provider)

export default firebase
